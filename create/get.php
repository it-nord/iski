<?
//Пример использования классов при генерации HTML
use F5Studio\CH;
use F5Studio\RHtml;
use F5Studio\DataMapper\IBlock\Isk;
use F5Studio\DataMapper\IBlock\IskType;
use F5Studio\Parser\ParserHelper;
require ($_SERVER ["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle ( "create" );
$pHelper = new ParserHelper();
$criteria = array (
		'filter' => array (
				"ID" => 4 
		) 
);

$isk = new Isk ();
$arIsk = $isk->getItem ( $criteria, true );
$arJson = CH::getJsonDecodeWithoutBom($arIsk ['FIELDS'] ['~VALUE'] ['TEXT'] );
$criteria = array (
		'filter' => array (
				"ID" => $arIsk ['ISK_TYPE'] ['VALUE'] 
		) 
);
$iskType = new IskType ();
$arType = $iskType->getItem ( $criteria, true );
echo "<pre>";
print_r($arType);
echo "</pre>";
echo "<pre>";
echo "</pre>";
$html = $pHelper->getTexBlock($arType ['HEADER'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div><table width=\"100%\"><tr><td></td><td width=\"250\"><div>", "</div></td></tr></table>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType ['COMMON_PART_START'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType ['COMMON_PART_BODY'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType ['COMMON_PART_END'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form'], true);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType ['COMMON_PART_ATTACH'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType ['SIGNATURE'] ['~VALUE'] ['TEXT'], $arJson['form'],"<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceSimpleTags($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
echo $html;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>