<?
use F5Studio\RHtml;
// use F5Studio;
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Создать иск");
$criteria = array(
    'filter' => array(
        "ID" => 3
    )
);
$iskType = new F5Studio\DataMapper\IBlock\IskType();
$arType = $iskType->getItem($criteria, true);
$arJson = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $arType['FORM_STEPS']['~VALUE']['TEXT']), true);
?>
Выберите тематику иска:<br/>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"isk_sections", 
	array(
		"IBLOCK_TYPE" => "isk",
		"IBLOCK_ID" => "2",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "2",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"VIEW_MODE" => "LINE",
		"SHOW_PARENT_NAME" => "Y"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>