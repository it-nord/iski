<?
use F5Studio\RHtml;
// use F5Studio;
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Создать иск");
$iskType = (isset($_GET['TYPE']) && $_GET['TYPE']!="") ? $_GET['TYPE'] : 0;
if ($iskType != 0) {
$criteria = array(
    'filter' => array(
        "ID" => $iskType,
    )
);
$iskType = new F5Studio\DataMapper\IBlock\IskType();
$arType = $iskType->getItem($criteria, true);
$arJson = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $arType['FORM_STEPS']['~VALUE']['TEXT']), true);
?>
Заполните форму:<br/>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	Array(
	)
);?>
<link rel="stylesheet" href="css/style.css" type="text/css"
	media="screen" />
<script type="text/javascript" src="sliding.form.js"></script>
<div id="content">
	<div id="wrapper">
			<div id="navigation_step" style="display: none;">
			<ul>
                    <?$i=0;foreach ($arJson as $step) {?>
                        <li <?if ($i==0){?> class="selected" <?}?>><a
					href="#"><?=$step['NAME']?></a></li>
                        <?$i++;}?>
                        <li><a href="#">Отправить</a></li>
			</ul>
		</div>
		<div id="steps">
			<form id="formElem" name="formElem" action="">
                    <?php foreach ($arJson as $step) {?>
                        <fieldset class="step">
					<legend><?=$step['QUEST']?></legend>
					<p>
								<?=$step['TIP']?>
							</p>
                            <?foreach ($step['FIELDS'] as $field) {?>
                            <?if ($field['TYPE'] == "RADIOBTN") {?>
                            <?
                                if (! empty($field['ANSWERS'])) {
                                    ?>
                                <p>
                                <label for="<?=$field['ID']?>"><?=$field['NAME']?></label>
                                <?
                                    foreach ($field['ANSWERS'] as $answer) {
                                        $attr["name"] = 'form['.$field["ID"].']';
                                        $attr["value"] = $answer[1];
                                        echo RHtml::getFormConstruction($field, $attr, $answer[1]);
                                    }
                                    ?>
                                </p>
                                <?
                                }
                            } else {?>
                            <p>
                            <?
                                $attr["name"] = ($field['MLT']!= "NO" && !empty($field['MLT'])) ? 'form['.$field["ID"].'][]' : 'form['.$field["ID"].']';
                                ?>
                            
						<label for="<?=$field['ID']?>"><?=$field['NAME']?></label>
                                <?=RHtml::getFormConstruction($field, $attr);
                                 //echo ($field['MLT'] == "multi") ?  '<input class="add-input" type="button" value="Добавить">' : "";?>
                                <!-- <input id="<?=$field['ID']?>" name="<?=$field['ID']?>" type="text"/> -->
					</p>
                            <?
                            }
                            $attr ="";
                        }
                        ?>
                        </fieldset>
                        <?php }?>
						<fieldset class="step">
					<legend>Отправить</legend>
					<p>Если каждый шаг отмечен зеленой подтверждающей иконкой, то в
						форме все поля заполнены правильно. Красная иконка указывает на
						наличие ошибок в некоторых полях ввода. На данном последнем шаге
						пользователь подтверждает правильность ввода информации.</p>
					<p class="submit">
						<button id="registerButton">Получить иск!</button>
					</p>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<?}
else echo "ВЫ НЕ ВЫБРАЛИ ТИП ИСКА!";?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>