<?php

namespace ItNord\RValidator;

/**
 * Class ItNord\RValidator\RUniqueValidator
 */
class RUniqueValidator extends RValidator
{
    /**
     * Валидация поля. Поле должно быть уникальным. Сначала проверяется текущий сценарий.
     *
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule)
    {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# должно быть уникальным';

        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();

        $count = $this->_model->getCount(
            array(
                $attrName => $value,
                'ACTIVE' => 'Y'
            )
        );

        if (isset($rule['on']) && in_array($scenario, $rule['on']) && $count) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }

        if (!isset($rule['on']) && $count) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}