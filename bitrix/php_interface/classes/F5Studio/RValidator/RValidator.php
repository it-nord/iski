<?php

namespace ItNord\RValidator;

class RValidator {
    /**
     * @var AbstractDataMapper $_model валидируемый объект
     */
    protected $_model;

    public function __construct($model) {
        $this->_model = $model;
    }

    public function validate() {
        $rules = $this->resolveRules();
        foreach ($rules as $attrName => $rule) {
            foreach ($rule as $ruleName => $ruleValue) {
                $validatorClass = $this->getValidatorClass($ruleName);
                $validator = new $validatorClass($this->_model);
                $validator->validateField($attrName, $ruleValue);
            }
        }
        return true;
    }

    public function getValidatorClass($validator) {
        return '\ItNord\RValidator\R' . ucfirst($validator) . 'Validator';
    }

    public function resolveRules() {
        $rules = $this->_model->rules();
        $attributesRules = array();
        foreach ($rules as $ruleAttributes => $rule) {
            if(!is_int($ruleAttributes)) {
                $attributes = preg_split('/[\s,]+/', $ruleAttributes, -1, PREG_SPLIT_NO_EMPTY);
            } else {
                $attributes = array();
            }

            $validators = array();
            foreach ($rule as $k => &$v) {
                if (!is_array($v) && is_int($k)) {
                    $validators[$v] = array();
                } else if (is_array($v)){
                    if (isset($v['on']) && !is_array($v['on'])) {
                        $v['on'] = preg_split('/[\s,]+/', $v['on'], -1, PREG_SPLIT_NO_EMPTY);
                    }
                    $validators[$k] = $v;
                } else {
                    $validators[$k] = array('param' => $v);
                }
            }


            foreach ($attributes as $attribute) {
                if (isset($attributesRules[$attribute])) {
                    $attributesRules[$attribute] = array_merge_recursive($attributesRules[$attribute], $validators);
                } else {
                    $attributesRules[$attribute] = $validators;
                }
            }

        }
        return $attributesRules;
    }

    protected function addError($attributeName, $errorMessage) {
        $this->_model->addError($attributeName, $errorMessage);
    }
}