<?php

namespace ItNord\RValidator;
use ItNord\CH;

class RUrlValidator extends RValidator {
    /**
     * Валидация поля. Поле должно быть валидным url'ом. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# должно быть валидным url адресом';
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && !empty($value) && !CH::checkUrlIsValid($value)) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && !empty($value) && !CH::checkUrlIsValid($value)) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}