<?php

namespace ItNord\RValidator;
use ItNord\CH;

class REmailValidator extends RValidator {
    /**
     * Валидация поля. Поле должно быть валидным e-mail'ом. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# должно быть валидным адресом электронной почты';
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && !empty($value) && !CH::checkEmailIsValid($value)) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && !empty($value) &&  !CH::checkEmailIsValid($value)) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}