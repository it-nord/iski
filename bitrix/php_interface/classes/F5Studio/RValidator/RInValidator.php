<?php

namespace ItNord\RValidator;

class RInValidator extends RValidator {
    /**
     * Валидация поля. Поле должно быть одним из. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (is_array($value)) {
            foreach ($value as $v) {
                $this->doValidate($rule, $scenario, $attrName, $v);
            }
        } else {
            $this->doValidate($rule, $scenario, $attrName, $value);;
        }
    }

    /**
     * Собственно валидация
     * @param $rule
     * @param $scenario
     * @param $attrName
     * @param $value
     */
    protected function doValidate($rule, $scenario, $attrName, $value) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Значение поля #FIELD# неверно';
        if (!in_array($value, $rule) && !empty($value)) {
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}