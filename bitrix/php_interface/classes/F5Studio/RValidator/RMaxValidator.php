<?php

namespace ItNord\RValidator;

class RMaxValidator extends RValidator {
    /**
     * Валидация поля. Поле не должно быть больше, чем параметр. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    public function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# не должно быть больше ' . $rule['param'];
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && !empty($value) && intval($value) > $rule['param']) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && !empty($value) && intval($value) > $rule['param']) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}