<?php

namespace ItNord\RValidator;

class RNumericalValidator extends RValidator {
    /**
     * Валидация поля. Поле должно быть числом. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# должно быть числом без пробелов';
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && !empty($value) && !is_numeric($value)) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && !empty($value) && !is_numeric($value)) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }

    }
}