<?php

namespace ItNord\RValidator;

class RLengthValidator extends RValidator {
    /**
     * Валидация поля. Поле должно быть в пределах заданного значения. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Длина поля #FIELD# не соответствует требованиям';
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        $param = $rule['param'];
        $paramSign = '=';
        $paramLength = 0;
        if (is_array($param)) {
            if (count($param) == 2) {
                $paramSign = $param[0];
                $paramLength = intval($param[1]);
            } else if (count($param) == 1) {
                $paramLength = intval($param[0]);
            }
        } else {
            $paramLength = intval($param);
        }

        $valueLength = 0;
        if (is_array($value)) {
            $valueLength = count($value);
        } else if (is_string($value)) {
            $valueLength = strlen($value);
        }

        // отношения обратны. Определяют, когда выдавать ошибку
        $map = array(
            ">" => $valueLength <= $paramLength,
            ">=" => $valueLength < $paramLength,
            "<" => $valueLength >= $paramLength,
            "<=" => $valueLength > $paramLength,
            "=" => $valueLength != $paramLength,
            "==" => $valueLength != $paramLength,
            "!=" => $valueLength == $paramLength,
        );

        if (isset($rule['on']) && in_array($scenario, $rule['on']) && $map[$paramSign]) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && $map[$paramSign]) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}