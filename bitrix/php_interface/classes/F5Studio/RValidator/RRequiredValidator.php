<?php

namespace ItNord\RValidator;

class RRequiredValidator extends RValidator {
    /**
     * Валидация поля. Поле не должно быть пустым. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# обязательно для заполнения';

        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (is_array($value)) {
            $value = array_filter($value, 'strlen');
        }
        if (is_string($value)) {
            $value = trim($value);
        }
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && (empty($value) || is_null($value))) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && (empty($value) || is_null($value))) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}