<?php

namespace ItNord\RValidator;

class REqualValidator extends RValidator {
    /**
     * Валидация поля. Поле не должно быть пустым. Сначала проверяется текущий сценарий.
     * @param $attrName
     * @param $rule
     */
    protected function validateField($attrName, $rule) {
        $errorMsg = (!empty($rule['message']))
            ? $rule['message']
            : 'Поле #FIELD# должно быть равно ' . $rule['param'];
        $value = $this->_model->getAttrValue($attrName);
        $scenario = $this->_model->getScenario();
        if (isset($rule['on']) && in_array($scenario, $rule['on']) && $value != $rule['param']) { // для текущего сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
        if (!isset($rule['on']) && $value != $rule['param']) { // для любого сценария
            $this->addError($attrName, $errorMsg);
            return;
        }
    }
}