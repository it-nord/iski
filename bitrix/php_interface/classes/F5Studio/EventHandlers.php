<?php


namespace F5Studio;

/**
 *
 */
class EventHandlers
{
    /**
     * Обновляет время последнего доступа к сайту для отображения кто online, кто нет
     */
    public function setLastActivityDate()
    {
        if ($GLOBALS['USER']->IsAuthorized()) {
            \CUser::SetLastActivityDate($GLOBALS['USER']->GetID());
        }
    }


}