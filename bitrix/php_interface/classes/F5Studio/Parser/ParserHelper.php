<?php
namespace F5Studio\Parser;

/**
 * Вспомогательный класс для чтения/парсинга DOCX и последующей генерации PDF
 *
 * @author user
 *        
 */
class ParserHelper
{

    private $keyForSum = "";

    public function __construct()
    {}

    /**
     * Метод заменяет вхождения полей для суммирования результатом сложения (значение)
     * Поля вида (10+20+30) заменяются на 60
     *
     * @param string $string            
     * @param array $arSum            
     */
    public function replaceMathExpressions($string, array $arFields)
    {
        preg_match_all("/\[(.*?)\]/i", $string, $arMath);
        foreach ($arMath[1] as $math) {
            if (preg_match('/(\+)/s', $math)) {
                $keyForSum = $math;
                $summands = explode('+', $math);
            }
        }
        foreach ($arFields as $key => $value) {
            if (in_array($key, $summands)) {
                $arSum[] = $value;
            }
        }
        $arMathematica[$keyForSum] = array_sum($arSum);
        foreach ($arMathematica as $k => $v) {
            $out = str_replace('[' . $k . ']', $v, $string);
        }
        return $out;
    }

    /**
     * Метод возвращает массив со слагаемыми для дальнейшей обработки.
     *
     * В строке находятся выражения вида (a+b+c+...) и разбираются на слагаемые.
     *
     * @param string $string            
     * @return multitype:
     */

    public function replaceSimpleTags($string, array $arFields)
    {
        foreach ($arFields as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $v) {
                    $string = str_replace('[' . $key . ']', $v, $string);
                }
            } else
                $string = str_replace('[' . $key . ']', $value, $string); // замена простых тегов
        }
        return $string;
    }

    /**
     * Метод возвращает текст с замененными условиями
     *
     * @param string $string            
     * @param array $arFields            
     * @return mixed
     */
    public function replaceDefinitions($string, array $arFields, $debug = false)
    {
        preg_match_all("/\[IF.*?\](.*?)\[\/ELSE\]/i", $string, $arIf);
        $ifElse = html_entity_decode($arIf[0][0]);
        preg_match("/\[IF(.*?)\](.*?)\[\/IF\]/i", $ifElse, $arIfElse[]);
        preg_match("/\[ELSE\](.*?)\[\/ELSE\]/i", $ifElse, $arIfElse[]);
        $uslovie = $arIfElse[0][1];
        preg_match("/([A-Z_]+)(.)(.)/i", html_entity_decode($uslovie), $arUslovie);
        if (array_key_exists($arUslovie[1], $arFields)) {
            if (is_array($arFields[$arUslovie[1]])) {
                $count = count($arFields[$arUslovie[1]]);
                if ($count < $arUslovie[3]) {
                    $string = str_replace($arIf[0][0], $arIfElse[0][2], $string);
                } else
                    $string = str_replace($arIf[0][0], $arIfElse[1][1], $string);
            } else
                $string = str_replace($arIf[0][0], $arIfElse[1][1], $string);
        }
        if ($debug) {
            print_r($arIfElse);
        }
        return $string;
    }

    /**
     * Метод возвращает отформатированный текстовый блок для дальнейшей обработки
     *
     * @param string $textBlock            
     * @param string $startTags            
     * @param string $endTags            
     */
    public function getTexBlock($textBlock, $arFields, $startTags = "", $endTags = "")
    {
        $out = $startTags;
        $out .= $textBlock;
        $out .= $endTags;
        // $out = $this->replaceSimpleTags($out, $arFields);
        // $out .= $this->getDefinitions($out, $arFields);
        // $out = $this->getRepStrings($out, $arFields);
        // $out = $this->getNumList($out);
        $out = str_replace(PHP_EOL, '<br/>', $out);
        return $out;
    }

    /**
     * Метод возвращает строку с отформатированным нумерованным списком для генератора PDF
     *
     * @param string $string            
     * @return string
     */
    public function getNumList($string)
    {
        preg_match_all('/\[N\](.*?)\[\/N\]/is', $string, $matches);
        $numList = explode(PHP_EOL, $matches[1][0]);
        // print_r($numList);
        $i = 0;
        foreach ($numList as $li) {
            $i ++;
            if (! empty($li)) {
                $out .= $i . ". " . $li;
            }
            $out .= PHP_EOL;
        }
        $out = str_replace($matches[0][0], $out, $string);
        return $out;
    }

    /**
     * Метод возвращает замену для повторяющихся фрагментов
     *
     * @param string $string            
     * @param array $arFields            
     * @return string
     */
    public function replaceRepStrings($string, array $arFields)
    {
        $count = 0;
        if (preg_match_all('/\[R\|(.*?)\s\]/is', $string, $matches)) {
            preg_match_all('/\[(.*?)]/is', $matches[1][0], $tags);
            $arIn = $tags[1];
            foreach ($arFields as $key => $value) {
                if (is_array($value)) {
                    if (in_array($key, $arIn)) {
                        $count = count($value);
                        foreach ($value as $v) {
                            $arIn2[$key][] = $v;
                        }
                    }
                }
            }
            for ($i = 0; $i < $count; $i ++) {
                $out .= $matches[1][0];
                $out .= PHP_EOL;
                $out .= PHP_EOL;
            }
            $string = str_replace($matches[0][0], $out, $string);
        }
        return $string;
    }
}