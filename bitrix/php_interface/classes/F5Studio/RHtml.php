<?php

namespace F5Studio;
/**
 * Хелпер для html-форм. Создание тегов и т.п.
 */
class RHtml {

   

    /**
     * label для формы
     * @param $label
     * @param array $attributes
     * @return string
     */
    public function label($label, $attributes = array()) {
        return self::tag('label', $attributes, $label, true);
    }

    /**
     * Текстовое поле или пароль
     * Можно передавать в $attributes 'type' => 'password'
     * @param array $attributes Аттрибуты поля
     * @return string
     */
    public static function textField($attributes = array()) {
        $attributes['type'] = (!empty($attributes['type']))
            ? $attributes['type']
            : 'text';
        $attributes['value'] = html_entity_decode($attributes['value'], ENT_QUOTES, 'UTF-8');
        return self::tag('input', $attributes, '', false);
    }

    /**
     * Текстовая область
     * VALUE передается в $attributes["value"]
     * @param array $attributes Аттрибуты текстовой области
     * @return string
     */
    public static function textarea($attributes = array()) {
        $value = '';
        if (isset($attributes["value"])) {
            $value = $attributes["value"];
            unset($attributes["value"]);
        }
        return self::tag('textarea', $attributes, $value, true);
    }

    /**
     * Выпадающий список
     * @param $attributes
     * @param $listData
     * @param bool $multiselect
     * @return string
     */
    public static function dropDownList($attributes, $listData, $multiselect = false) {
        $list = '';
        if (!empty($attributes['empty'])) {
            if (!is_array($attributes['empty'])) {
                $attributes['empty'] = array('empty' => $attributes['empty']);
            }
            foreach ($attributes['empty'] as $k => $v) {
                $optionAttributes = array('value' => $k);
                $list .= self::tag('option', $optionAttributes, $v);
            }
            unset($attributes['empty']);
        }

        $attributes['id'] = isset($attributes['id'])
            ? $attributes['id']
            : str_replace(array('[', ']'), '_', $attributes['name']);;

        $attributes['value'] = (is_array($attributes['value']))
            ? $attributes['value']
            : preg_split('/[\s,]+/', $attributes['value'], -1, PREG_SPLIT_NO_EMPTY);

        foreach ($listData as $k => $v) {
            $optionAttributes = array('value' => $k);
            // значение по умолчанию
            if (in_array($k, $attributes['value'])) {
                $optionAttributes['selected'] = 'selected';
            }
            $list .= self::tag('option', $optionAttributes, $v);
        }
        if ($multiselect === true) {
            //Для сохранения мультиселектов в ИБ битрикс нужен массив вида $CODE => array( 0 => Array("VALUE" => $ENUM_ID )),
            // в то время как для сохранения обычных списков нужет $CODE => Array("VALUE" => $ENUM_ID)
            //для того, чтобы в запросе приходил массив, и по ключу можно было определить, что это обычных селект.
            $attributes['multiple'] = 'multiple';
            $attributes['name'] .= '[]';
        }
        unset($attributes['value']);
        unset($attributes['type']);
        return self::tag('select', $attributes, $list);
    }

    /**
     * Список кнопок (радиокнопки)
     * @param $attributes
     * @param $listData
     * @return string
     */
    public static function btnsList($attributes, $listData) {
        $list = '';
        $attributes['value'] = (is_array($attributes['value']))
            ? $attributes['value']
            : preg_split('/[\s,]+/', $attributes['value'], -1, PREG_SPLIT_NO_EMPTY);

        $listAttributes = (isset($attributes['listAttributes']))
            ? $attributes['listAttributes']
            : array();
        $listClass = (isset($listAttributes['class']))
            ? 'btn_group__btn ' . $listAttributes['class']
            : 'btn_group__btn';
        $listAttributes['href'] = (isset($listAttributes['href']))
            ? $listAttributes['href']
            : '#';
        $listClass .= ($listAttributes['href'] === '#')
            ? ' btn_group__link'
            : '';

        foreach ($listData as $k => $v) {
            $listAttributes['data-value'] = $k;
            $listAttributes['class'] = $listClass;
            // значение по умолчанию
            if (in_array($k, $attributes['value'])) {
                $listAttributes['class'] .= ' current btn_group__btn-toggle';
            }
            $list .= self::tag('a', $listAttributes, $v);
        }

        $attributes['class'] = (isset($attributes['class']))
            ? 'btn_group ' . $attributes['class']
            : 'btn_group';
        unset($attributes['value']);
        unset($attributes['type']);
        unset($attributes['listAttributes']);
        return self::tag('div', $attributes, $list);
    }

    /**
     * Список чекбоксов
     * @param $attributes
     * @param $listData
     * @return string
     */
    public static function checkboxList($attributes, $listData) {
        $html = '';
        $html .= self::openTag('div', array('class' => 'cb_wrap'));

        $attributes['value'] = (is_array($attributes['value']))
            ? $attributes['value']
            : preg_split('/[\s,]+/', $attributes['value'], -1, PREG_SPLIT_NO_EMPTY);

        foreach ($listData as $k => $v) {
            $checkBoxAttributes = $attributes;
            $checkBoxAttributes['name'] = $attributes['name'] . '[' . $k . ']';
            $checkBoxAttributes['id'] = $checkBoxAttributes['name'];
            $checkBoxAttributes['value'] = $k;
            if (in_array($k, $attributes['value'])) {
                $checkBoxAttributes['checked'] = 'checked';
            }

            $hiddenFieldAttributes = $checkBoxAttributes;
            unset ($hiddenFieldAttributes['value']);
            unset ($hiddenFieldAttributes['checked']);
            $hiddenFieldAttributes['type'] = 'hidden';
            $html .= self::textField($hiddenFieldAttributes);
            $label = $v;
            $html .= self::checkbox($checkBoxAttributes, $label);
        }
        $html .= self::closeTag('div');
        return $html;
    }

    /**
     * Чекбокс с подписью
     * @param $attributes
     * @param $label
     * @param string $separator
     * @return string
     */
    public static function checkbox($attributes, $label, $separator = '<br>') {
        $attributes['type'] = 'checkbox';
        $html = self::tag('input', $attributes);
        $html .= self::tag('label', array('for' => $attributes['id']), $label);
        $html .= $separator;
        return $html;
    }
    
    /**
     * Радиобатон
     * @param $attributes
     * @param $label
     * @param string $separator
     * @return string
     */
    public static function radio($attributes, $label, $separator = '<br>') {
        $attributes['type'] = 'radio';
        $html = self::tag('input', $attributes);
        $html .= $label;
        //$html .= self::tag('label', array('for' => $attributes['id']), $label);
        $html .= $separator;
        return $html;
    }

    /**
     * HTML-тег
     * @param $tagName
     * @param array $attributes
     * @param mixed $content
     * @param bool $closeTag
     * @return string
     */
    public static function tag($tagName, $attributes = array(), $content = false, $closeTag = true) {
        $html = '<' . $tagName . self::renderAttributes($attributes);
        if ($content === false) {
            return $closeTag
                ? $html . ' />'
                : $html . '>';
        } else {
            return $closeTag
                ? $html . '>' . $content . '</' . $tagName . '>'
                : $html . '>' . $content;
        }
    }

    /**
     * Открывающий тег
     * @param $tagName
     * @param array $attributes
     * @return string
     */
    public static function openTag($tagName, $attributes = array()) {
        return '<' . $tagName . self::renderAttributes($attributes) . '>';
    }

    /**
     * Закрывающий тег
     * @param $tagName
     * @return string
     */
    public static function closeTag($tagName) {
        return '</' . $tagName . '>';
    }

    /**
     * Отображение атрибутов в теге
     * @param $attributes
     * @return string
     */
    protected static function renderAttributes($attributes) {
        if (empty($attributes)) {
            return '';
        }
        $html = '';
        foreach ($attributes as $name => $value) {
            if ($value) {
                if ($value == 'empty') {
                    $value = '';
                }
                $html .= ' ' . $name . '="' . htmlentities($value, ENT_QUOTES, "UTF-8") . '"';
            }
        }
        return $html;
    }
    
    /**
     * Создает конструкцию для формы создания иска по входящему массиву, описывающему поле.
     * 
     * @param array $field массив с описанием поля
     * 
     */
    public static function getFormConstruction (array $field, array $attr=array(), $label = "") {
        if ($field['TYPE'] == TEXTEDIT) {
            $outString=self::textField($attr);
        }
        elseif ($field['TYPE'] == CHECKBOX) {
            $outString=self::checkbox($attr);
        }
        elseif ($field['TYPE'] == RADIOBTN) {
            $outString=self::radio($attr,$label);
        }
        else $outString=""; 
        return $outString;
        
    }


}