<?php
/**
 * PDF Generator class file
 *
 * @author Evgeny Semonenko <e.semonenko@gmail.com>
 */
namespace F5Studio\IskGenerator;
require_once ($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/classes/tcpdf/tcpdf.php');
class PDFGenerator extends \TCPDF
{

    public function createPDFFromHtml($html, $iskId)
    {
        set_time_limit(0);
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // add a page
        $pdf->AddPage();
        // set font
        $pdf->SetFont('times', '', 14);
        // output the HTML content
        $pdf->writeHTML($html, true, 0, true, true);
        // ---------------------------------------------------------
        // Close and output PDF document
        $pdf->Output('/create/'.$iskId.'.pdf', 'F');
        return true;
    }
}