<?php
/**
 * CH (CommonHelper) class file
 *
 * @author Evgeny Semonenko <e.semonenko@gmail.com>
 */

namespace F5Studio;

class CH
{

    /**
     * Обрезает текст
     *
     * @param string $string   строка, которую требуется обрезать
     * @param int    $maxlen   максимальная необходимая длина
     * @param bool   $byWord   обрезать по слову. Необязательный, по умолчанию true, обрезает по концу слов
     * @param string $ellipsis многоточие или другой символ в конце. Необязательный, по умолчанию отсутствует
     *
     * @return string обрезанная строка
     */
    public static function cutString($string, $maxlen, $byWord = true, $ellipsis = '')
    {
        if ($byWord) {
            $len = (mb_strlen($string) > $maxlen)
                ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
                : $maxlen;
        } else {
            $len = (mb_strlen($string) > $maxlen)
                ? $maxlen
                : mb_strlen($string);
        }
        if ($len >= mb_strlen($string)) {
            $ellipsis = '';
        }

        return mb_substr($string, 0, $len) . $ellipsis;
    }

    /**
     * Обертка над CFile::MakeFileArray
     *
     * Получает путь к файлу от корня веб-сервера или ID файла, позволяет задавать произольное имя файла
     *
     * @param string $filepath Путь к файлу ОТ КОРНЯ ВЕБ-СЕРВЕРА или ID файла
     * @param string $name     Имя файла, необязательно, по умолчанию - '', т.е. имя исходного (временного) файла
     * @param bool   $mimetype MIME-тип
     *
     * @static
     * @see CFile::MakeFileArray
     * @return array|bool|null
     */
    public static function makeFileArray($filepath, $name = '', $mimetype = false)
    {
        if (!is_numeric($filepath) && strpos($filepath, 'http://') === false) {
            $filepath = $_SERVER['DOCUMENT_ROOT'] . $filepath;
        }
        $arFile = \CFile::MakeFileArray($filepath, $mimetype);
        if (is_array($arFile) && !empty($arFile)) {
            $arFile['name'] = ($name !== '')
                ? $name
                : $arFile['name'];
        }
        return $arFile;
    }

    /**
     * Проверяет локальный хост для разработки или сервер в интернете
     *
     * @static
     * @return bool
     */
    public static function isLocalHost()
    {
        $url = parse_url($_SERVER['HTTP_HOST']);
        $host = ($url['path'] && !$url['host'])
            ? $url['path']
            : $url['host'];
        $suffix = substr($host, strrpos($host, '.'));
        return in_array(
            $suffix,
            array(
                '.dev',
                '.local',
            )
        );
    }

    /**
     * Проверка на валидность и существование домена url. Кириллические домены преобразуются в punycode и проверяеются
     *
     * @param $url проверяемый url
     *
     * @static
     * @return bool
     */
    public static function checkUrlIsValid($url)
    {
        $idn = new idna_convert(array('idn_version' => 2008));
        $url = (stripos($url, 'xn--') !== false)
            ? $idn->decode($url)
            : $idn->encode($url);
        $url = (stripos($url, 'http://') !== false)
            ? $url
            : 'http://' . $url;
        $urlData = parse_url($url);
        $host = $urlData['host'];
        $hostWithScheme = 'http://' . $host;
        return (filter_var($hostWithScheme, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED)
                && self::checkDomainExists(
                $host
            ));
    }

    /**
     * Преобразует массив в строку в формате массива javascript
     *
     * @param $array массив для преобразования
     *
     * @static
     * @return string строка в формате javascript
     */
    public static function phpArrayToJsArray($array)
    {
        $out = '[';
        foreach ($array as $k => $v) {
            $out .= ($out !== '[')
                ? ','
                : '';
            $out .= '[' . $k . ', ' . $v . ']';
        }
        $out .= ']';
        return $out;
    }

    /**
     * Возвращает число прописью в виде "пятнадцать рублей 36 копеек"
     *
     * @param float $num число (сумма)
     *
     * @return string сумма прописью
     */
    public static function priceToString($num)
    {
        $nul = 'ноль';
        $ten = array(
            array(
                '',
                'один',
                'два',
                'три',
                'четыре',
                'пять',
                'шесть',
                'семь',
                'восемь',
                'девять'
            ),
            array(
                '',
                'одна',
                'две',
                'три',
                'четыре',
                'пять',
                'шесть',
                'семь',
                'восемь',
                'девять'
            ),
        );
        $a20 = array(
            'десять',
            'одиннадцать',
            'двенадцать',
            'тринадцать',
            'четырнадцать',
            'пятнадцать',
            'шестнадцать',
            'семнадцать',
            'восемнадцать',
            'девятнадцать'
        );
        $tens = array(
            2 => 'двадцать',
            'тридцать',
            'сорок',
            'пятьдесят',
            'шестьдесят',
            'семьдесят',
            'восемьдесят',
            'девяносто'
        );
        $hundred = array(
            '',
            'сто',
            'двести',
            'триста',
            'четыреста',
            'пятьсот',
            'шестьсот',
            'семьсот',
            'восемьсот',
            'девятьсот'
        );
        $unit = array( // Units
                       array(
                           'копейка',
                           'копейки',
                           'копеек',
                           1
                       ),
                       array(
                           'рубль',
                           'рубля',
                           'рублей',
                           0
                       ),
                       array(
                           'тысяча',
                           'тысячи',
                           'тысяч',
                           1
                       ),
                       array(
                           'миллион',
                           'миллиона',
                           'миллионов',
                           0
                       ),
                       array(
                           'миллиард',
                           'милиарда',
                           'миллиардов',
                           0
                       ),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                } # 20-99
                else {
                    $out[] = $i2 > 0
                        ? $a20[$i3]
                        : $ten[$gender][$i3];
                } # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = self::formatByCount($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        $out[] = self::formatByCount(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . self::formatByCount($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return self::ucfirst(trim(preg_replace('/ {2,}/', ' ', join(' ', $out))));
    }

    /**
     * Склонение существительных после чисел
     *
     * Наприме 1 стол, 2 стола, 10 столов
     *
     * @param int    $count число
     * @param string $form1 первая форма
     * @param string $form2 вторая форма
     * @param string $form3 третья форма
     *
     * @return string
     */
    public static function formatByCount($count, $form1, $form2, $form3)
    {
        $count = abs($count) % 100;
        $lcount = $count % 10;
        if ($count >= 11 && $count <= 19) {
            return ($form3);
        }
        if ($lcount >= 2 && $lcount <= 4) {
            return ($form2);
        }
        if ($lcount == 1) {
            return ($form1);
        }
        return $form3;
    }

    /**
     * Увеличение первой буквы текста для многобайтовой кодировки
     *
     * @param string $string строка
     * @param string $enc    кодировка
     *
     * @static
     * @return string
     */
    public static function ucfirst($string, $enc = 'utf-8')
    {
        return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc) . mb_substr(
            $string,
            1,
            mb_strlen($string, $enc),
            $enc
        );
    }

    /**
     * Определение кодировки строки
     *
     * @param $string
     *
     * @static
     * @return null
     */
    public static function detectEncoding($string)
    {
        static $list
        = array(
            'utf-8',
            'windows-1251'
        );

        foreach ($list as $item) {
            $sample = iconv($item, $item, $string);
            if (md5($sample) == md5($string)) {
                return $item;
            }
        }
        return null;
    }


    /**
     * Обработчик ошибок преобразования JSON
     *
     * @param int $error Код ошибки
     *
     * @static
     */
    public static function jsonError($error)
    {
        switch ($error) {
            case JSON_ERROR_NONE:
                echo ' - Ошибок нет';
                break;
            case JSON_ERROR_DEPTH:
                echo ' - Достигнута максимальная глубина стека';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Некорректные разряды или не совпадение режимов';
                break;
            case JSON_ERROR_CTRL_CHAR:
                echo ' - Некорректный управляющий символ';
                break;
            case JSON_ERROR_SYNTAX:
                echo ' - Синтаксическая ошибка, не корректный JSON';
                break;
            case JSON_ERROR_UTF8:
                echo ' - Некорректные символы UTF-8, возможно неверная кодировка';
                break;
            default:
                echo ' - Неизвестная ошибка';
                break;
        }
    }


    /**
     * Проверяет, что число находится в заданном диапазоне
     *
     * @param int $val Число
     * @param int $min Минимум
     * @param int $max Максимум
     *
     * @static
     * @return bool
     */
    public static function inRange($val, $min, $max)
    {
        return ($val >= $min && $val <= $max);
    }
    
    /**
     * Метод возвращает результат исполнения json_decode без управляющих BOM 
     * @param string $jsonArray
     */
    public static function getJsonDecodeWithoutBom($jsonArray) {
        $arOut = json_decode ( preg_replace ( '/[\x00-\x1F\x80-\xFF]/', '', $jsonArray ), true );
        return $arOut;
    }
    
  
}