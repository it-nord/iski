<?php


namespace F5Studio;

/**
 * Класс для вывода сообщений или ошибок на экран.
 * Все сообщения собираются в одно.
 * Есть разделение разных типов событий
 * Также можно задавать название для события.
 * События с разными названиями показываются отдельно, но в одном общем блоке.
 */
class Message
{
    const ERROR = 'errortext';
    const MESSAGE = 'notetext';
    const SUCCESS = 'successtext';
    static $messageType
        = array(
            self::ERROR => 'Внимание!',
            self::MESSAGE => 'Внимание!',
            self::SUCCESS => 'Поздравляем!',
        );
    protected static $instances;
    protected $messages = array();

    /**
     * Закрыли конструктор
     */
    protected function __construct()
    {
    }

    /**
     * Получение экземпляра класса
     *
     * @return Message
     */
    public static function getInstance()
    {
        $class = get_called_class();
        if (!isset(self::$instances[$class])) {
            self::$instances[$class] = new $class;
        }
        return self::$instances[$class];
    }

    /**
     * Добавление сообщения в список
     *
     * @param string $message Сообщение
     * @param string $eventName Название события сообщения
     * @param string $type Тип сообщения
     */
    public function addMessage($message, $type = self::MESSAGE, $eventName = 'flash')
    {
        $message = trim($message);
        if ($message != '' && !in_array($message, $this->messages[$type][$eventName])) {
            $this->messages[$type][$eventName][] = $message;
        }
    }

    /**
     * Вывод сообщений в браузер
     */
    public function render()
    {
        $messagesHtml = '';
        if (!empty($this->messages)) {
            foreach ($this->messages as $messageType => $messages) {
                $messageHtml = '';
                foreach ($messages as $eventName => $message) {
                    $messageHtml .= self::messageHtml($message);
                }
                $messagesHtml .= self::messagesHtml($messageHtml, $messageType);
            }

            if (SITE_TEMPLATE_ID == 'print') {
                $mess = $messagesHtml;
            } else {
                $mess
                    = "<script type='text/javascript'>
                $(document).ready(function(){
                    $('#messages').html('{$messagesHtml}').fadeIn('fast');
                    var messagesTop = $('#messages').offset().top - 50;
                    $('html,body').animate ({scrollTop: messagesTop},500);
                    $('#messages').on('click', 'a.close', function(e) {
                        $('#messages').fadeOut('fast', function() {
                            $(this).html('');
                        });
                        e.preventDefault();
                    });
                })
            </script>";
            }

            echo $mess;
        }
    }

    /**
     * Шаблон вывода сообщений (контейнер)
     *
     * @param array $message
     *
     * @return string
     */
    protected function messageHtml($message)
    {
        return '<div class="message__text">' . implode('<br>', $message) . '</div>';
    }

    /**
     * Шаблон для каждого сообщения
     *
     * @param $message
     * @param $type
     *
     * @return string
     */
    protected function messagesHtml($message, $type)
    {
        $typeText = self::$messageType[$type];
        $output = '<div class="c3"><div class="message message-' . $type . '">';
        $output .= '<div class="message__content"><div class="message__content__header">' . $typeText . '</div>';
        $output .= '<div class="message__content__body">' . html_entity_decode($message, ENT_QUOTES, 'UTF-8')
                   . '</div>';
        $output .= '<a class="close" href="#"></a>';
        $output .= '</div></div></div>';
        return $output;
    }

    /**
     * Возвращает true если есть сообщения
     *
     * @return bool
     */
    public function messagesExists()
    {
        return (!empty($this->messages));
    }

    /**
     * Закрыли клонирование
     */
    final private function __clone()
    {
    }
}