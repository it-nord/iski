<?
namespace F5Studio\DataMapper\IBlock;

use \F5Studio\DataMapper\IBlock\IBlockDataMapper;

/**
 * Class Poshlina госпошлины
 */
class IskType extends IblockDataMapper
{

    protected $iblockId = 2;

    /**
     * Возвращает модель текущего класса
     *
     * @param string $className            
     *
     * @static
     *
     * @return \F5Studio\\DataMapper\IBlock\IBlockDataMapper модель класса IblockDataMapper
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

  
}