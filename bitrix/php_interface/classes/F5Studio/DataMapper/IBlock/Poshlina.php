<?
namespace F5Studio\DataMapper\IBlock;

use \F5Studio\DataMapper\IBlock\IBlockDataMapper;

/**
 * Class Poshlina госпошлины
 */
class Poshlina extends IblockDataMapper
{

    protected $iblockId = 1;

    /**
     * Возвращает модель текущего класса
     *
     * @param string $className            
     *
     * @static
     *
     * @return \F5Studio\\DataMapper\IBlock\IBlockDataMapper модель класса IblockDataMapper
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Рассчитывает госпошлину по суммe иска
     *
     * @param int $sum
     *            Сумма иска
     *            
     * @static
     *
     * @return int
     *
     */
    public function getGPbySum($sum)
    {
        $criteria = array(
            'filter' => array(
                array(
                    "LOGIC" => "AND",
                    ">=PROPERTY_MAX_SUMMA" => $sum,
                    "<=PROPERTY_MIN_SUMMA" => $sum
                )
            )
        );
        $arPoshlina = $this->getItem($criteria, true);
        $gp = 0;
        $minSum = intval($arPoshlina['MIN_SUMMA']['VALUE']);
        $maxSum = intval($arPoshlina['MAX_SUMMA']['VALUE']);
        $statGP = intval($arPoshlina['STATIC_GP']['VALUE']);
        $plusPercent = intval($arPoshlina['PLUS_PROCENT']['VALUE']);
        $percentOverhead = intval($arPoshlina['PROCENT_OVERHEAD']['VALUE']);
        $minGP = intval($arPoshlina['MIN_GP']['VALUE']);
        $maxGP = intval($arPoshlina['MAX_GP']['VALUE']);
        if ($statGP > 0) {
            $gp += $statGP;
        }
        if ($percentOverhead > 0 && $sum > $percentOverhead) {
            $gp += ($sum - $percentOverhead) * $plusPercent / 100;
        } else {
            $gp += $sum * $plusPercent / 100;
        }
        if ($minGP > 0 && $gp < $minGP) {
            $gp = $minGP;
        }
        if ($maxGP > 0 && $gp > $maxGP) {
            $gp = $maxGP;
        }
        return $gp;
    }
}