<?php

namespace F5Studio\DataMapper\IBlock;

use \F5Studio\DataMapper\AbstractDataMapper;

/**
 * Class IblockDataMapper
 */
class IBlockDataMapper extends AbstractDataMapper
{
    /**
     * @var int $iblockId Идентификатор инфоблока
     */
    protected $iblockId;
    protected $_attributes = array();

    /**
     * Возвращает модель текущего класса
     *
     * @param string $className
     *
     * @static
     * @return IblockDataMapper модель класса IblockDataMapper
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Конструктор
     *
     * Подключаем модуль iblock
     */
    public function __construct($scenario = 'insert')
    {
        parent::__construct($scenario);
        if (!\CModule::IncludeModule('iblock')) {
            throw new Exception('Невозможно загрузить модуль ИнфоБлоков');
            return false;
        }
    }

    /**
     * Обновляет только указанное свойство
     *
     * @param string   $code   Код свойства
     * @param mixed    $value  Значение свойства
     * @param int|null $itemId Идентификатор элемента. Необязательный, по умолчанию data[ID]
     *
     * @return bool
     */
    public function updateProperty($code, $value, $itemId = null)
    {
        if (is_null($itemId)) {
            $itemId = $this->getAttrValue('ID');
        }
        $itemId = intval($itemId);

        if (!$itemId) {
            return false;
        }

        \CIBlockElement::SetPropertyValuesEx($itemId, $this->iblockId(), array($code => $value));
        $this->clearCache();
        return true;
    }

    /**
     * Устанавливает идентификатор темы форума для комментариев
     *
     * @param int    $elementId Идентификатор элемента
     * @param int    $value     Идентификатор темы форума
     * @param string $code      Код свойства, необязательный, по умолчанию 'TID'
     */
    public function setCommentTopicId($elementId, $value, $code = 'TID')
    {
        \CIBlockElement::SetPropertyValuesEx($elementId, $this->iblockId(), array($code => $value));
        $this->clearCache();
    }

    /**
     * Возвращает идентификатор инфоблока
     *
     * @return int
     */
    public function iblockId()
    {
        return $this->iblockId;
    }

    /**
     * Добавляет новой значение в множественно свойство
     *
     * @param int | array $item  Массив с полями элемента или идентификатор элемента
     * @param string      $code  Код свойства
     * @param string      $value Новое значение
     *
     * @return bool
     */
    public function addToMultipleProperty($item, $code, $value)
    {
        if (is_numeric($item)) {
            $item = $this->getItem($item, true);
        }

        if ($item[$code]['MULTIPLE'] !== 'Y') {
            return false;
        }
        $oldValue = $item[$code]['~VALUE'];
        if ($oldValue != '' && !is_array($oldValue)) {
            return false;
        }
        if (!in_array($value, $oldValue)) {
            $oldValue[] = $value;
            $newValue = $this->modifyAttributeValueByType($item[$code], $oldValue);

            \CIBlockElement::SetPropertyValuesEx($item['ID'], $this->iblockId(), array($code => $newValue));
        }

        $this->clearCache();

        return true;
    }

    /**
     * Удаляет значение из множественного свойства
     *
     * @param int|array $item  Массив с полями элемента или Идентификатор элемента
     * @param string    $code  Код свойства
     * @param mixed     $value Значение
     *
     * @return bool
     */
    public function removeFromMultipleProperty($item, $code, $value)
    {
        if (is_numeric($item)) {
            $item = $this->getItem($item, true);
        }
        $id = $item['ID'];

        if ($item[$code]['MULTIPLE'] !== 'Y') {
            return false;
        }
        $oldValue = $item[$code]['~VALUE'];
        if ($oldValue != '' && !is_array($oldValue)) {
            return false;
        }
        if (($key = array_search($value, $oldValue)) !== false) {
            unset($oldValue[$key]);
            $newValue = $this->modifyAttributeValueByType($item[$code], $oldValue);
            if (empty($newValue)) {
                $newValue = '';
            }

            \CIBlockElement::SetPropertyValuesEx($id, $this->iblockId(), array($code => $newValue));
        }

        $this->clearCache();

        return true;
    }

    /**
     * Возвращает массив с полями элемента
     *
     * @param array|int $criteria массив с параметрами для поиска элемента или идентификатор элемента
     *                            Если передать пустой массив, то вернется элемент с минимальным значением поля SORT и созданный последним
     * @param bool      $withProperties
     *
     * @return array
     */
    public function getItem($criteria, $withProperties = false)
    {
        if (is_numeric($criteria)) {
            $id = intval($criteria);
            $criteria = array(
                'filter' => array(
                    'ID' => $id
                ),
            );
        } else if (is_array($criteria)) {
            $criteria['filter'] = (is_array($criteria['filter']))
                ? $criteria['filter']
                : array();
            $criteria['select'] = (is_array($criteria['select']))
                ? $criteria['select']
                : array();
        } else {
            return array();
        }
        $criteria['limit'] = 1;
        $withProperties = filter_var($withProperties, FILTER_VALIDATE_BOOLEAN);
        $items = $this->getItems($criteria, $withProperties);
        return array_shift(array_values($items));
    }

    /**
     * Обертка для \CIBlockElement::GetList
     *
     * По умолчанию задаются некоторые параметры фильтра, сортировки и т.д. Возвращает массив со всеми элементами,
     * удовлетворяющими критериям поиска
     *
     * @param array  $criteria       Критерий поиска. Не обязательное, по умолчанию array - все элементы
     * @param bool   $withProperties Получать свойства или нет. Не обязательное, по умолчанию false - не получать свойства
     * @param string $key            Поле-ключ выходного массива
     *
     * @return array
     */
    public function getItems($criteria = array(), $withProperties = false, $key = 'ID')
    {
        if (!is_array($criteria)) {
            return array();
        }

        $criteria = $this->sanitizeCriteria($criteria);
        $cacheId = md5(serialize($criteria) . $withProperties . $key);

        extract($criteria);
        /** @var array $arFilter */
        /** @var array $arSort */
        /** @var array|false $groupBy */
        /** @var array|false $limit */
        /** @var array $arSelect */

        $arFilter['IBLOCK_ID'] = (is_array($arFilter['IBLOCK_ID']))
            ? $arFilter['IBLOCK_ID']
            : $this->iblockId;
        $withProperties = filter_var($withProperties, FILTER_VALIDATE_BOOLEAN);
        $items = array();

        if ($this->obCache->InitCache($this->cacheTime, $cacheId, $this->cachePath)) {
            $vars = $this->obCache->GetVars();
            $items = $vars[$cacheId];
        } elseif ($this->obCache->StartDataCache()) {

            $dbList = \CIBlockElement::GetList($arSort, $arFilter, $groupBy, $limit, $arSelect);
            while ($ob = $dbList->GetNextElement()) {
                $arItem = $ob->GetFields();
                if ($withProperties) {
                    $arProperties = $ob->GetProperties();
                    $propertiesList = array_keys($arProperties);
                    $arItem = array_merge($arItem, $arProperties);
                    $arItem['PROPERTIES_LIST'] = $propertiesList;
                }
                $this->afterFind($arItem);
                $arItem['DATA_MAPPER_CLASS'] = get_class($this);
                if ($arItem[$key]) {
                    $itemsKey = $arItem[$key];
                    if (is_array($itemsKey) && array_key_exists('VALUE', $itemsKey)) {
                        $itemsKey = $itemsKey['VALUE'];
                    }
                    $items[$itemsKey] = $arItem;
                } else {
                    $items[] = $arItem;
                }
            }
            $this->obCache->EndDataCache(array($cacheId => $items));
        }
        return $items;
    }

    /**
     * Получение названия элемента по его id. Ползет в базу, не стоит выполнять в цикле.
     *
     * @param $id
     *
     * @return mixed
     */
    public function getElementNameById($id)
    {
        $item = $this->getItem($id);
        return $item['NAME'];
    }


    /**
     * Изменение значений свойств в формат Битрикса в зависимости от типа свойства
     *
     * @param array $item
     * @param mixed $value значение
     *
     * @internal param string $code название атрибута
     * @return mixed значение атрибута
     */
    protected function modifyAttributeValueByType($item, $value)
    {
        $type = $item['PROPERTY_TYPE'];
        switch ($type) {
            case 'L':
                if (is_array($value)) {
                    if ($item['MULTIPLE'] == 'Y') {
                        foreach ($value as $k => $v) {
                            $value[] = array(
                                'VALUE' => $v,
                                'DESCRIPTION' => $item['DESCRIPTION'][$k],
                            );
                        }
                    } else {
                        $value = array(
                            array(
                                'VALUE' => reset($value),
                                'DESCRIPTION' => $item['DESCRIPTION'],
                            ),
                        );
                    }
                } else {
                    $value = array(
                        array(
                            'VALUE' => $item['VALUE_ENUM_ID'],
                            'DESCRIPTION' => $item['DESCRIPTION'],
                        )
                    );
                }

                break;
            case 'S':
                if ($item['USER_TYPE'] == 'HTML') {
                    $value = array('VALUE' => $value);
                }
                break;
        }
        return $value;
    }

    /**
     * Возвращает массив вида $value => $label
     *
     * @param string $value
     * @param string $label
     * @param array  $items
     *
     * @return array
     */
    public function getList($value, $label = '', $items = array())
    {
        $label = ($label != '')
            ? $label
            : $value;
        $criteria = array(
            'select' => array(
                $value,
                $label
            ),
        );
        if (empty($items) || !is_array($items)) {
            $items = $this->getItems($criteria);
        }
        $resultList = array();
        foreach ($items as $item) {
            $resultList[$item[$value]] = $item[$label];
        }
        return $resultList;
    }

    /**
     * Получение множественного свойства по коду
     *
     * @param $code
     *
     * @return array
     */
    public function getPropEnum($code)
    {
        $propertyEnums = \CIBlockPropertyEnum::GetList(
            Array(
                "SORT" => "ASC",
                "DEF" => "DESC"
            ),
            Array(
                "IBLOCK_ID" => $this->iblockId,
                "CODE" => $code
            )
        );
        $enum = array();
        while ($enumFields = $propertyEnums->GetNext()) {
            $enum[$enumFields["ID"]] = $enumFields["VALUE"];
        }
        return $enum;
    }

    /**
     * Получение списка свойств инфоблока (не значений).
     *
     * @param array $criteria Массив
     *
     * @return array
     */
    public function getProperty($criteria = array())
    {
        $result = array();
        $sort = ($criteria['sort'])
            ? $criteria['sort']
            : array();
        $filter = ($criteria['filter'])
            ? $criteria['filter']
            : array();
        $filter['IBLOCK_ID'] = $this->iblockId();
        $dbProps = \CIBlockProperty::GetList(
            $sort,
            $filter
        );
        while ($props = $dbProps->GetNext()) {
            $result[] = $props;
        }
        return $result;
    }

    /**
     * Получение раздела
     *
     * @param $criteria
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function getSection($criteria)
    {
        if (is_numeric($criteria)) {
            $id = intval($criteria);
            $criteria = array(
                'filter' => array(
                    'ID' => $id
                ),
            );
        } else if (is_array($criteria)) {
            $criteria['filter'] = (is_array($criteria['filter']))
                ? $criteria['filter']
                : array();
            $criteria['select'] = (is_array($criteria['select']))
                ? $criteria['select']
                : array();
        } else {
            return array();
        }
        $criteria['limit'] = 1;
        $items = $this->getSections($criteria);
        return array_shift(array_values($items));
    }

    /**
     * Получение списка разделов
     *
     * @param array $criteria
     *
     * @return array || bool
     * @throws Exception
     */
    public function getSections($criteria = array())
    {
        if (!is_array($criteria)) {
            throw new Exception('Данные для поиска разделов неверны');
            return false;
        }

        $cacheId = 'sec_' . md5(serialize($criteria));

        $arSort = (is_array($criteria['sort']))
            ? $criteria['sort']
            : array(
                "SORT" => "ASC",
                "NAME" => "ASC"
            );
        $arFilter = (is_array($criteria['filter']))
            ? $criteria['filter']
            : array();
        $arSelect = (is_array($criteria['select']))
            ? $criteria['select']
            : array();

        if (is_array($criteria['limit'])) {
            $limit = $criteria['limit'];
        } else if (is_numeric($criteria['limit'])) {
            $limit = array("nTopCount" => $criteria['limit']);
        } else {
            $limit = false;
        }
        $bIncCnt = ($criteria['bIncCnt'])
            ? $criteria['bIncCnt']
            : false;
        $arFilter['IBLOCK_ID'] = $this->iblockId();

        $sections = array();


        if ($this->obCache->InitCache($this->cacheTime, $cacheId, $this->cachePath)) {
            $vars = $this->obCache->GetVars();
            $sections = $vars[$cacheId];
        } elseif ($this->obCache->StartDataCache()) {
            $dbList = \CIBlockSection::GetList($arSort, $arFilter, $bIncCnt, $arSelect, $limit);
            while ($arSection = $dbList->GetNext()) {
                $sections[$arSection['ID']] = $arSection;
            }
            $this->obCache->EndDataCache(array($cacheId => $sections));
        }
        return $sections;
    }

    /**
     * Получает количество разделов, удовлетворяющих фильтру
     *
     * @param array $filter
     *
     * @return string
     * @throws Exception
     */
    public function getSectionsCount($filter = array())
    {
        if (!is_array($filter)) {
            throw new Exception('Фильтр должен быть массивом');
            return;
        }
        $filter['IBLOCK_ID'] = $this->iblockId();
        return \CIBlockSection::GetCount($filter);
    }

    /**
     * Сохранение раздела
     *
     * @param $arFields
     *
     * @return bool|int
     */
    public function saveSection($arFields)
    {
        $requiredFields = array(
            'IBLOCK_ID' => $this->iblockId(),
        );
        $arFields = array_merge($arFields, $requiredFields);

        $bs = new \CIBlockSection;

        $id = intval($arFields['ID']);
        unset($arFields['ID']);
        if ($id > 0) {
            $res = $bs->Update($id, $arFields);
        } else {
            $id = $bs->Add($arFields);
            $res = ($id > 0);
        }

        if (!$res) {
            $this->lastError = $bs->LAST_ERROR;
        } else {
            $this->clearCache();
        }
        return $id;
    }

    /**
     * Деактивация элементов пачкой
     *
     * @param $criteria
     *
     * @return array
     */
    public function deactivateItems($criteria, $clear = array())
    {
        $items = $this->getItems($criteria);
        $deactivateIds = array();
        foreach ($items as $item) {
            $deactivateIds[] = $item['ID'];
        }
        global $DB;
        $idsArray = array_chunk($deactivateIds, 300);

        $clearSql = '';
        if (!empty($clear)) {
            foreach ($clear as $v) {
                $clearSql = ' , `' . $v . '`=""';
            }
        }

        foreach ($idsArray as $ids) {
            $sql = 'UPDATE `b_iblock_element` SET `ACTIVE`="N"' . $clearSql . ' WHERE `ID` IN (' . implode(',', $ids)
                   . ')';
            $DB->Query($sql);
        }
        $this->clearCache();
        return $deactivateIds;
    }

    /**
     * Получает количество элементов пользователя
     *
     * @param null   $uid     идентификатор пользователя. Необязательное, по умолчанию null - текущий пользователь
     *
     * @param string $groupBy Свойство для группировки, например 'PROPERTY_USER_ID'. Необязательное, по умолчанию CREATED_BY - создатель элемента
     *
     * @return string
     */
    public function getCountByUser($uid = null, $groupBy = 'CREATED_BY')
    {
        if (is_null($uid) || !is_numeric($uid)) {
            global $USER;
            $uid = $USER->GetID();
        }
        $filter = array(
            $groupBy => $uid,
            'ACTIVE' => 'Y'
        );
        return $this->getItemsCount($filter);
    }

    /**
     * Получает количество элементов, удовлетворяющих фильтру
     *
     * @param array $filter
     *
     * @return string
     * @throws Exception
     */
    public function getItemsCount($filter = array())
    {
        if (!is_array($filter)) {
            throw new Exception('Фильтр должен быть массивом');
            return;
        }
        $filter['IBLOCK_ID'] = $this->iblockId();
        return \CIBlockElement::GetList(array(), $filter, array());
    }

    /**
     * Получает максимальное количество элементов на сайте
     *
     * @param string $groupBy Свойство для группировки, например 'PROPERTY_USER_ID'. Необязательное, по умолчанию CREATED_BY - создатель элемента
     *
     * @return mixed
     */
    public function getMaxCount($groupBy = 'CREATED_BY')
    {
        $criteria = array(
            'sort' => array(
                $groupBy => 'ASC',
            ),
            'filter' => array(
                'ACTIVE' => 'Y'
            ),
            'group' => array(
                $groupBy,
            ),
        );
        $items = $this->getItems($criteria);
        $itemsCount = array();
        foreach ($items as $item) {
            $itemsCount[] = $item['CNT'];
        }
        return max($itemsCount);
    }

    /**
     * Удаление всех неактивных элементов.
     *
     * Удаляет используя функцию self::delete(), т.е. с проверкой прав.
     * Для каждого неактивного элемента делается запрос в базу и проверка прав доступа,
     * поэтому удаление большого числа элементов неэффективно.
     *
     */
    public function deleteInactive()
    {
        global $USER;
        $items = $this->getItems(
            array(
                'filter' => array(
                    'ACTIVE' => 'N',
                    'CREATED_BY' => $USER->GetID(),
                ),
                'select' => array(
                    'ID',
                ),
            )
        );
        foreach ($items as $item) {
            $this->delete($item['ID']);
        }
    }

    /**
     * Удаление элемента с проверкой прав.
     *
     * @param int|array $item Идентификатор  или массив с параметрами удаляемого элемента
     *
     * @return bool
     */
    public function delete($item)
    {
        $status = false;
        if ($this->canEdit($item)) {
            if (is_array($item) && !empty($item['ID'])) {
                $id = $item['ID'];
            } else {
                $id = $item;
            }
            $status = $this->deleteElement($id);
        }
        if ($status) {
            $this->afterDelete($item);
        }
        return $status;
    }

    /**
     * Проверка прав доступа к элементу.
     *
     * По умолчанию доступ предоставляется по совпадению поля CREATED_BY или администраторам.
     *
     * @param array|int $item Идентификатор элемента или массив - элемент
     *
     * @return bool
     */
    public function canEdit($item)
    {
        global $USER;
        if (is_numeric($item)) {
            $item = $this->getItem($item);
        }
        return ($item['CREATED_BY'] === $USER->GetID() || $USER->IsAdmin());
    }

    /**
     * Удаление элемента из базы
     *
     * @param int $id Идентификатор удаляемого элемента
     *
     * @return bool
     */
    protected function deleteElement($id)
    {
        global $DB;
        $status = false;
        $DB->StartTransaction();
        if (!\CIBlockElement::Delete($id)) {
            $DB->Rollback();
        } else {
            $status = true;
            $DB->Commit();
            $this->clearCache();
        }
        return $status;
    }

    /**
     * Удаление раздела с проверкой прав доступа
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteSection($id)
    {
        $status = false;
        if ($this->canEditSection($id)) {
            $status = \CIBlockSection::Delete($id, false);
            if ($status) {
                $this->clearCache();
            }
        }
        return $status;
    }

    /**
     * Проверка прав доступа к разделу.
     *
     * По умолчанию доступ предоставляется по совпадению поля CREATED_BY или администраторам.
     *
     * @param int|array $item Идентификатор или массив параметров элемента
     *
     * @return bool
     */
    public function canEditSection($item)
    {
        global $USER;
        if (is_numeric($item)) {
            $item = $this->getSection($item);
        }
        return ($item['CREATED_BY'] == $USER->GetID() || $USER->IsAdmin());
    }

    /**
     * Удаление файла из элемента с проверкой прав.
     *
     * @param int    $elementId      ID элемента
     * @param string $propertyCode   Код свойства
     * @param int    $propertyFileId PROPERTY_VALUE_ID удаляемого файла
     *
     * @return bool
     */
    public function deleteFile($elementId, $propertyCode, $propertyFileId)
    {
        $status = false;
        $arFile = array(
            "MODULE_ID" => "iblock",
            "del" => "Y",
        );
        if ($this->canEdit($elementId)) {
            $status = \CIBlockElement::SetPropertyValueCode(
                $elementId,
                $propertyCode,
                Array($propertyFileId => Array("VALUE" => $arFile))
            );
            if ($status) {
                $this->clearCache();
            }
        }
        return $status;
    }

    /**
     * Подписи полей
     *
     * @return array
     */
    protected function labels()
    {
        return array(
            'ID' => 'ID элемента',
            'CODE' => 'Мнемонический идентификатор',
            'XML_ID' => 'Внешний идентификатор',
            'NAME' => 'Название',
            'IBLOCK_ID' => 'ID информационного блока',
            'IBLOCK_SECTION_ID' => 'ID группы',
            'ACTIVE' => 'Флаг активности',
            'DATE_ACTIVE_FROM' => 'Дата начала действия',
            'DATE_ACTIVE_TO' => 'Дата окончания действия',
            'SORT' => 'Порядок сортировки',
            'PREVIEW_PICTURE' => 'Картинка предварительного просмотра (анонса)',
            'PREVIEW_TEXT' => 'Предварительное описание (анонс)',
            'PREVIEW_TEXT_TYPE' => 'Тип предварительного описания',
            'DETAIL_PICTURE' => 'Картинка для детального просмотра',
            'DETAIL_TEXT' => 'Детальное описание',
            'DETAIL_TEXT_TYPE' => 'Тип детального описания',
            'SEARCHABLE_CONTENT' => 'Содержимое для поиска при фильтрации групп',
            'DATE_CREATE' => 'Дата создания',
            'CREATED_BY' => 'Код пользователя, создавшего элемент',
            'CREATED_USER_NAME' => 'Имя пользователя, создавшего элемент',
            'TIMESTAMP_X' => 'Время последнего изменения полей',
            'MODIFIED_BY' => 'Код пользователя, изменившего элемент',
            'USER_NAME' => 'Имя пользователя, изменившего элемент',
            'LANG_DIR' => 'Путь к папке сайта',
            'LIST_PAGE_URL' => 'Шаблон URL-а к странице для публичного просмотра списка элементов информационного блока',
            'DETAIL_PAGE_URL' => 'Шаблон URL-а к странице для детального просмотра элемента',
            'SHOW_COUNTER' => 'Количество показов',
            'SHOW_COUNTER_START' => 'Дата первого показа',
            'WF_COMMENTS' => 'Комментарий администратора документооборота',
            'WF_STATUS_ID' => 'Код статуса элемента в документообороте',
            'LOCK_STATUS' => 'Текущее состояние блокированности на редактирование',
            'TAGS' => 'Теги',
        );
    }

    /**
     * Сохранение элемента
     *
     * @return bool
     */
    protected function saveElement()
    {
        global $USER;
        $element = new \CIBlockElement;
        // загружаем аттрибуты
        $this->loadAttributes();
        $this->setAttrValue('MODIFIED_BY', $USER->GetID(), true);

        if ($this->isNewElement()) { // если это новый элемент
            // устанавливаем стандрартные значения свойств, если они не были заданы ранее

            $this->setAttrValue('ACTIVE', 'Y', true);
            $this->setAttrValue('NAME', 'Элемент', true);
            $propertyValues = $this->getAttrValue('PROPERTY_VALUES');
            // модифицируем значения свойств под формат битрикса
            if (!empty($propertyValues) && is_array($propertyValues)) {
                $this->modifyPropertyValues($this->_attributes, $propertyValues);
            }
        } else { // если обновляем старый
            // получаем данные существующего элемента
            $item = $this->getItem($this->getAttrValue('ID'), true);
            $oldDataPropertyValues = $this->oldPropertyValues($item);
            // объединяем существующие данные с новыми
            $newPropertyValues = $this->getAttrValue('PROPERTY_VALUES');
            // модифицируем значения свойств под формат битрикса
            if (!empty($newPropertyValues) && is_array($newPropertyValues)) {
                $propertyValues = array_merge($oldDataPropertyValues, $newPropertyValues);
            } else {
                $propertyValues = $oldDataPropertyValues;
            }
            // модифицируем значения свойств под формат битрикса
            $this->modifyPropertyValues($item, $propertyValues);
        }

        // модифицируем свойства типа "файл" в формат битрикса
        $this->modifyPropertyFileValues($propertyValues);
        // удаляем старые файлы
        $this->deleteOldFiles();

        if ($this->isNewElement()) {
            $this->setAttrValue('IBLOCK_ID', $this->iblockId());
            $productId = $element->Add($this->getData(), false, false);
            $eventName = 'OnAfterAdd' . get_class($this);
        } else {
            $isSuccess = $element->Update($this->getAttrValue('ID'), $this->getData());
            $productId = ($isSuccess)
                ? $this->getAttrValue('ID')
                : 0;
            $eventName = 'OnAfterUpdate' . get_class($this);
        }
        if ($productId) {
            $this->setAttrValue('ID', $productId);
            $this->clearCache();

            // вызов битриксового события
            $rsEvents = GetModuleEvents('itnord.mnb', $eventName);
            $model = $this;
            while ($arEvent = $rsEvents->Fetch()) {
                ExecuteModuleEventEx($arEvent, compact('model'));
            }
            return true;
        } else {
            $this->lastError = $element->LAST_ERROR;
            return false;
        }
    }

    /**
     * Загрузка аттрибутов инфоблока
     */
    protected function loadAttributes()
    {
        $dbProperties = \CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $this->iblockId()));
        while ($arProperty = $dbProperties->Fetch()) {
            if ($arProperty['CODE']) {
                $this->_attributes[$arProperty['CODE']] = $arProperty;
            } else {
                $this->_attributes[$arProperty['ID']] = $arProperty;
            }
        }
    }

    /**
     * Модифицирует значения свойств в формат битрикса
     *
     * @param array $item   Массив свойств инфоблока
     * @param array $values Значения свойств инфоблока
     */
    protected function modifyPropertyValues($item, $values)
    {
        $modifiedValues = array();
        foreach ($values as $code => $value) {
            $modifiedValues[$code] = $this->modifyAttributeValueByType($item[$code], $value);
        }
        $this->setAttrValue('PROPERTY_VALUES', $modifiedValues);
    }

    /**
     * Получает значения свойств существующего элемента. Используется при обновлении элемента
     *
     * @param $item
     *
     * @return array
     */
    protected function oldPropertyValues($item)
    {
        $properties = $item['PROPERTIES_LIST'];
        $data = array();
        foreach ($properties as $propertyName) {
            $prop = $item[$propertyName];
            if (is_array($prop) && isset($prop['VALUE']) && !empty($prop['CODE'])) {
                $data[$prop['CODE']] = $prop['VALUE'];
            }
        }
        return $data;
    }

    /**
     * Модифицирует свойства типа файл в формат битрикса
     */
    protected function modifyPropertyFileValues()
    {
        $fileProps = array();
        foreach ($this->_attributes as $attr) {
            if ($attr['PROPERTY_TYPE'] == 'F') {
                $fileProps[] = $attr['CODE'];
            }
        }
        $this->modifyAttrsTypeFiles($fileProps);
    }

    /**
     * Удаляет старые файлы
     */
    protected function deleteOldFiles()
    {
        $data = $this->getData();
        if (empty($data['ID'])) {
            return;
        }
        if (isset($data['DELETE']) && is_array($data['DELETE'])) {
            $delete = array_filter($data['DELETE']);
            if (!empty($delete)) {
                foreach ($delete as $code => $fileIds) {
                    $fileIds = array_filter($fileIds);
                    foreach ($fileIds as $fileId) {
                        $status = $this->deleteFile($data['ID'], $code, $fileId);
                    }
                }
            }
        }
        return true;
    }

    /**
     * Деактивация элемента с проверкой прав доступа
     *
     * @param $id
     *
     * @return bool
     */
    protected function deactivate($id)
    {
        $status = false;
        if ($this->canEdit($id)) {
            $element = new \CIBlockElement();
            $status = $element->Update($id, array('ACTIVE' => 'N'), false, false);
        }
        if ($status) {
            $this->clearCache();
        }
        return $status;
    }

    /**
     * Активация элемента с проверкой прав доступа
     *
     * @param $id
     *
     * @return bool
     */
    protected function activate($id)
    {
        $status = false;
        if ($this->canEdit($id)) {
            $element = new \CIBlockElement();
            $status = $element->Update($id, array('ACTIVE' => 'Y'), false, false);
            if ($status) {
                $this->clearCache();
            }
        }
        return $status;
    }
}