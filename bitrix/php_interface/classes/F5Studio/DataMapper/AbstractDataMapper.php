<?php

namespace F5Studio\DataMapper;

use F5Studio\RValidator\RValidator;
use F5Studio\CH;
use F5Studio\Message;

/**
 * Class AbstractDataMapper
 */
abstract class AbstractDataMapper
{

    protected static $_models = array();
    public $errors = array();
    protected $scenario;
    protected $lastError;
    protected $data = array();
    protected $attrNames = array();

    protected $cacheTime = 3600;
    protected $cachePath;
    protected $obCache;

    /**
     * Конструктор
     *
     */
    public function __construct($scenario = 'insert')
    {
        $this->obCache = new \CPHPCache();
        $this->cachePath = $this->getCachePath();

        if ($scenario === null) {
            return;
        }
        $this->setScenario($scenario);
    }

    /**
     * Возвращает модель класса
     *
     * @param string $className
     *
     * @static
     * @return mixed
     */
    public static function model($className = __CLASS__)
    {
        if (isset(self::$_models[$className])) {
            return self::$_models[$className];
        } else {
            $model = self::$_models[$className] = new $className(null);
            return $model;
        }
    }

    /**
     * @param int $cacheTime
     */
    public function setCacheTime($cacheTime)
    {
        $this->cacheTime = $cacheTime;
    }

    /**
     * @return int
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /**
     * @return string
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * @param string $scenario
     */
    public function setScenario($scenario)
    {
        $this->scenario = $scenario;
    }

    /**
     * Получает значение атрибута по коду
     *
     * @param $attrCode
     *
     * @return null
     */
    public function getAttrValue($attrCode)
    {
        $data = $this->getData();
        $value = null;
        if (isset($data[$attrCode])) {
            $value = $data[$attrCode];
        } else if(isset($data['PROPERTY_VALUES'][$attrCode])) {
            $value = $data['PROPERTY_VALUES'][$attrCode];
        }
        return $value;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        foreach ($data as $k => $v) {
            $this->setAttrValue($k, $v);
            if ($k == 'PROPERTY_VALUES') {
                $this->setData($v);
            }
        }
    }

    public function clearData()
    {
        $this->data = array();
    }

    /**
     * Устанавливаем атрибут модели
     *
     * @param string $code Код атрибута
     * @param mixed  $value Значение атрибута
     * @param bool   $soft Не заменять значение, если оно уже установлено
     * @param bool   $isProp Строго задает, что данный атрибут - это свойство
     */
    public function setAttrValue($code, $value, $soft = false, $isProp = false)
    {

        $value = (!empty($value) && $value !== 'empty')
            ? $value
            : '';
        if ($soft) {
            $value = ($this->data[$code])
                ? : $value;
            $value = ($this->data['PROPERTY_VALUES'][$code])
                ? : $value;
        }

        if (isset($this->data['PROPERTY_VALUES'][$code]) || $isProp) {
            $this->data['PROPERTY_VALUES'][$code] = $value;
        } else {
            $this->data[$code] = $value;
        }
    }

    /**
     * Удаляет свойство объекта
     *
     * @param $code
     */
    public function unsetAttr($code)
    {
        unset($this->data[$code]);
        if (isset($this->data['PROPERTY_VALUES'][$code])) {
            unset($this->data['PROPERTY_VALUES'][$code]);
        }

    }

    /**
     * Проверяет наличие свойства в объекте
     *
     * @param $code
     *
     * @return bool
     */
    public function issetAttr($code)
    {
        return (isset ($this->data[$code]) || isset($this->data['PROPERTY_VALUES'][$code]));
    }

    /**
     * Модификация данных перед валидацией.
     *
     * @return bool
     */
    protected function beforeValidate()
    {
        return true;
    }

    /**
     * Модификация данных перед сохранением.
     *
     * @return bool
     */
    protected function beforeSave()
    {
        // обработка свойств типа файл
        $fileAttrs = array('PREVIEW_PICTURE', 'DETAIL_PICTURE');
        $this->modifyAttrsTypeFiles($fileAttrs);

        return true;
    }

    /**
     * Выполнение действий после сохранения элемента
     *
     */
    protected function afterSave()
    {
    }

    /**
     * Выполнение действий после нахождения елемента
     */
    protected function afterFind(&$item)
    {
    }

    /**
     * Выполнение действий после удаления елемента
     *
     * @param $item
     */
    protected function afterDelete(&$item)
    {
    }

    /**
     * Обработка свойств типа "файл" перед их сохранением в базу
     *
     * @param array $attrs Массив названий свойств типа "файл"
     */
    protected function modifyAttrsTypeFiles($attrs) {
        foreach ($attrs as $attr) {
            if ($this->issetAttr($attr)) {
                $value = $this->getAttrValue($attr);
                // если свойсво файл - это число, то значит оно уже сохранено, удаляем его, чтобы не пересоздавать новое
                if (is_numeric($value)) {
                    $this->unsetAttr($attr);
                } else if (is_array($value)) {
                    // имеем множественное свойство, запускаем цикл
                    $files = array();
                    foreach ($value as $k => $v) {
                        if (is_numeric($v)) {
                            continue;
                        }
                        $name = '';
                        $nameAttr = $attr . '_name';
                        if ($this->issetAttr($nameAttr)) {
                            $names = $this->getAttrValue($nameAttr);
                            $name = $names[$k];
                        }
                        $files[] = CH::makeFileArray($v, $name);
                    }
                    $this->setAttrValue($attr, $files);
                } else {
                    // записываем имя в массив. Битрикс определяет тип файл по имени, поэтому можно словить ошибку "Неверный тип файла"
                    $name = '';
                    $nameAttr = $attr . '_name';
                    if ($this->issetAttr($nameAttr)) {
                        $name = $this->getAttrValue($nameAttr);
                    }
                    $this->setAttrValue($attr, CH::makeFileArray($value, $name));
                }
            }
        }
    }


    /**
     * Валидация
     *
     * @return bool
     */
    public function validate()
    {
        if ($this->beforeValidate()) {
            $validator = new RValidator($this);
            $validator->validate();
            return (count($this->errors) == 0)
                ? true
                : false;
        }
        return false;
    }

    /**
     * Добавляет сообщение об ошибке атрибуту
     *
     * @param $attrName
     * @param $errorMessage
     */
    public function addError($attrName, $errorMessage)
    {
        if (!empty($this->errors[$attrName])) {
            $this->errors[$attrName] .= ' ' . $errorMessage;
        } else {
            $this->errors[$attrName] = $errorMessage;
        }
    }

    /**
     * Геттер сообщения об ошибке по имени атрибута
     *
     * @param $attributeName
     *
     * @return string
     */
    public function getErrorMessage($attributeName)
    {
        return isset($this->errors[$attributeName])
            ? $this->errors[$attributeName]
            : '';
    }

    /**
     * Возращает сразу все ошибки
     *
     * @return array
     */
    public function getErrorMessages()
    {
        $errors = array();
        foreach ($this->errors as $attrCode => $errorMessage) {
            $errors[$attrCode] = str_replace('#FIELD#', '"' . $this->getAttrName($attrCode) . '"', $errorMessage);
        }
        $errors['LAST_ERROR'] = $this->getLastError();
        return $errors;
    }

    /**
     * Получает имя свойства
     *
     * @param $attrCode
     *
     * @return mixed
     */
    public function getAttrName($attrCode)
    {

        if (!isset($this->attrNames[$attrCode])) {
            $this->findAttrNames();
        }
        return $this->attrNames[$attrCode];
    }

    /**
     * Получает имена всех свойств
     */
    protected function findAttrNames()
    {
        $cacheId = 'attrNames';


        if ($this->obCache->InitCache($this->cacheTime, $cacheId, $this->cachePath)) {
            $vars = $this->obCache->GetVars();
            $attrNames = $vars[$cacheId];
        } elseif ($this->obCache->StartDataCache()) {
            $attrNames = $this->labels();
            $properties = \CIBlockProperty::GetList(
                array("sort" => "asc", "name" => "asc"),
                array("ACTIVE" => "Y", "IBLOCK_ID" => $this->iblockId())
            );
            while ($propFields = $properties->GetNext()) {
                $attrNames[$propFields["CODE"]] = $propFields["NAME"];
            }
            $this->obCache->EndDataCache(array($cacheId => $attrNames));
        }
        $this->attrNames = $attrNames;
    }

    /**
     * Геттер последнего сообщения об ошибке при сохранении элемента
     *
     * @return string
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Проверяет, новый элемент или уже существующий
     *
     * @return bool
     */
    public function isNewElement()
    {
        $data = $this->getData();
        return (isset($data['ID']) && is_numeric($data['ID']))
            ? false
            : true;
    }

    /**
     * Правила валидации формы
     *
     * @return array
     */
    public function rules()
    {
        return array();
    }

    /**
     * Очищает стек ошибок
     */
    protected function clearErrorMessages()
    {
        $this->errors = array();
        $this->lastError = '';
    }

    /**
     * Сохранение
     *
     * @param bool $validate Требуется валидация или нет
     *
     * @return bool
     */
    public function save($validate = true) {
        $this->clearErrorMessages();
        if ($validate) {
            if (!$this->validate()) {
                $this->showValidateErrors();
                return false;
            }
        }

        if ($this->beforeSave()) {
            if ($this->saveElement()) {
                $this->afterSave();
                return true;
            }
        }

        Message::getInstance()->addMessage(
            'Возникли ошибки при сохранении',
            Message::ERROR
        );
        return false;
    }

    /**
     * Переводит свойства из массива item в массив data объекта
     *
     * @param $item
     *
     * @throws Exception
     */
    public function itemPropertiesToModel($item)
    {
        if (!is_array($item)) {
            throw new Exception('Элемент должен быть массивом');
        }

        foreach ($item as $code => $value) {
            if (!$this->isSafeProperty($code)) {
                continue;
            }
            if (is_array($value) && array_key_exists('VALUE', $value) && array_key_exists('CODE', $value) && !empty($value['CODE'])) {
                $this->setAttrValue($value['CODE'], $value['VALUE']);
            } else {
                $this->setAttrValue($code, $value);
            }
        }
    }

    /**
     * Проверяет разрешено ли свойство для сохранения или оно служебное
     *
     * @param $code
     *
     * @return bool
     */
    protected function isSafeProperty($code)
    {
        $excludedProperties = array(
            'DATE_CREATE',
            'DATE_CREATE_UNIX',
            'SEARCHABLE_CONTENT',
            'IBLOCK_TYPE_ID',
            'IBLOCK_CODE',
            'TMP_ID',
            'LID',
            'IBLOCK_NAME',
            'IBLOCK_EXTERNAL_ID',
            'DETAIL_PAGE_URL',
            'LIST_PAGE_URL',
            'BP_PUBLISHED',
            'USER_NAME',
            'TIMESTAMP_X',
            'TIMESTAMP_X_UNIX',
            'MODIFIED_BY',
            'CREATED_BY',
            'LOCK_STATUS',
            'SHOW_COUNTER',
            'SHOW_COUNTER_START',
            'LOCKED_USER_NAME',
            'CREATED_USER_NAME',
            'LANG_DIR',
            'CREATED_DATE',

        );
        $stopWords = array(
            '~',
            'WF',
        );
        // если код свойства начинается со стоп-слова, то свойство не нужно
        foreach ($stopWords as $word) {
            if (strpos($code, $word) === 0) {
                return false;
            }
        }

        // если код свойства в массиве исключенных свойств, то свойство тоже не нужно
        if (in_array($code, $excludedProperties)) {
            return false;
        }

        return true;
    }

    protected function saveElement()
    {

    }

    /**
     * Возвращает css-класс для строки формы при наличии ошибки
     *
     * @param string $attr Аттрибут, для которого проверяется наличие ошибки
     * @param string $class CSS-класс. Необязательное. По умолчанию 'form__row-error'
     *
     * @return string
     */
    public function errorCssClass($attr, $class = 'form__row-error')
    {
        if (is_array($attr)) {
            foreach ($attr as $v) {
                if (!empty($this->errors[$v])) {
                    return $class;
                }
            }
        } else {
            return (!empty($this->errors[$attr]))
                ? $class
                : '';
        }
    }

    /**
     * Обработка критерия поиска для getItems
     *
     * @param array $criteria
     *
     * @return array
     */
    protected function sanitizeCriteria($criteria) {
        $result = array();

        $result['arSort'] = (is_array($criteria['sort']))
            ? $criteria['sort']
            : array("SORT" => "ASC", "NAME" => "ASC");

        $result['arFilter'] = (is_array($criteria['filter']))
            ? $criteria['filter']
            : array();

        $result['groupBy'] = ($criteria['group'])
            ? $criteria['group']
            : false;

        if (is_array($criteria['limit'])) {
            $limit = $criteria['limit'];
        } else if (is_numeric($criteria['limit'])) {
            $limit = array("nTopCount" => $criteria['limit']);
        } else {
            $limit = false;
        }
        $result['limit'] = $limit;

        $result['arSelect'] = (is_array($criteria['select']))
            ? $criteria['select']
            : array();
        return $result;
    }


    /**
     * Очищает кэш дата-маппера
     */
    public function clearCache()
    {
        $this->obCache->CleanDir('DataMapper' . '/' . str_replace('\\', '/', get_class($this)));
    }

    /**
     * Возвращает путь к файлу кэша
     *
     * @return string
     */
    protected function getCachePath()
    {
        return  'DataMapper' . '/' . str_replace('\\', '/', get_class($this));
    }

    /**
     * Заменяет в строке код атрибута вида #ATTR_NAME# на его значение
     *
     * @param string $string Исходная строка
     *
     * @return mixed
     */
    public function replaceAttrsInString($string)
    {
        $data = $this->getData();
        dump($data);
        dump($string);
        foreach ($data as $attrKey => $attrValue) {
            if (is_array($attrValue) && $attrKey == 'PROPERTY_VALUES') {
                foreach ($attrValue as $propKey => $propValue) {
                    $string = str_replace('#' . $propKey . '#', $this->getAttrValue($propKey), $string);
                }
            } else {
                $string = str_replace('#' . $attrKey . '#', $this->getAttrValue($attrKey), $string);
            }
        }

        return $string;
    }

}