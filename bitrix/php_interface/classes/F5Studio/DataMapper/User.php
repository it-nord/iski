<?php
/**
 * User class file
 *
 * @author Evgeny Semonenko <e.semonenko@gmail.com>
 */

namespace F5Studio\DataMapper;

use \F5Studio\DataMapper\AbstractDataMapper;

/**
 * User - это класс, в котором собраны полезные функции для работы с пользователями
 *
 * Класс User является наследником DataMapper, но переопределяет многие функции, так как работает не с инфоблоками,
 * а с данными пользователя и пользовательскими свойствами.
 * Содержит константы определяющие роли пользователей (группы)
 *
 * @author Sergey Zelenov <serwizz@gmail.com>
 */
class User extends AbstractDataMapper
{
    /**
     * Администратор
     */
    const ROLE_ADMIN = 1;
    /**
     * Зарегистрированный пользователь
     */
    const ROLE_USER = 5;

    /**
     * @var array Времменое хранение ролей пользователей
     */
    protected static $userRoles = array();
    /**
     * @var array соответствие роли и текстового кода
     */
    public static $_roles
        = array(
            self::ROLE_ADMIN => 'admin',
            self::ROLE_USER => 'user',
        );
    /**
     * @var array Временное хранение профилей пользователей
     */
    protected $profiles = array();

    /**
     * Возвращает модель текущего класса
     * @param string $className
     * @static
     * @return IblockDataMapper модель класса IblockDataMapper
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * Возвращает идентификатор основной роли пользователя
     *
     * @param int  $uid   ID пользователя
     * @param bool $force Обязательно делать запрос к базе или взять из self::$_userRoles
     *
     * @return int|string
     */
    public function getUserRole($uid, $force = false)
    {
        $uid = intval($uid);
        if (!isset(self::$userRoles[$uid]) || $force) {
            $arGroups = \CUser::GetUserGroup($uid);
            foreach (self::$_roles as $roleId => $role) {
                if (in_array($roleId, $arGroups)) {
                    self::$userRoles[$uid] = intval($roleId);
                }
            }
        }
        return isset(self::$userRoles[$uid])
            ? self::$userRoles[$uid]
            : null;
    }

    /**
     * Возвращает текстовый код роли пользователя
     *
     * @param $uid
     *
     * @return bool
     */
    public function getUserRoleCode($uid)
    {
        $userRoleId = $this->getUserRole($uid);
        return isset(self::$_roles[$userRoleId])
            ? self::$_roles[$userRoleId]
            : false;
    }

    /**
     * Проверяет принадлежность пользователя роли
     *
     * @param int $uid идентификатор пользователя
     * @param int $role числовой идентификатор роли
     *
     * @return bool
     */
    public function equalRole($uid, $role) {
        $userRole = $this->getUserRole($uid);
        return ($userRole == $role);
    }

    /**
     * Проверяет принадлежность пользователя роли по коду роли
     *
     * @param int $uid идентификатор пользователя
     * @param string $roleCode строковый код роли
     *
     * @return bool
     */
    public function equalRoleCode($uid, $roleCode) {
        $userRoleCode = $this->getUserRoleCode($uid);
        return ($userRoleCode == $roleCode);
    }

    /**
     * Проверка прав доступа по роли
     *
     * @param int $role Идентификатор роли пользователя
     *
     * @return bool
     */
    public function checkRoleAccess($role) {
        global $USER;
        $uid = $USER->GetID();
        return ($this->equalRole($uid, $role) || $USER->IsAdmin());
    }

    /**
     * Возвращает данные пользователей согласно фильтра
     *
     * @param array $criteria  массив, в котором описаны параметры запроса
     * @param bool  $notAdmins - возвращать только не админов. По умолчанию false - возвращаются все типы пользователей
     * @param mixed $callback  Колбэк в который передается по ссылке каждый arUser. Необязательный, по умолчанию false
     *
     * @return array
     */
    public function getUsers($criteria = array(), $notAdmins = false, $callback = false)
    {

        $cacheId = md5(serialize($criteria) . $notAdmins . serialize($callback));

        $users = array();
        $arFilter = ($criteria['filter'])
            ? $criteria['filter']
            : array();
        $by = ($criteria['by'])
            ? $criteria['by']
            : 'id';
        $order = ($criteria['order'])
            ? $criteria['order']
            : 'desc';
        $arParameters = array();
        $arParameters['SELECT'] = ($criteria['select'])
            ? $criteria['select']
            : array();
        if (intval($criteria['limit'])) {
            $arParameters["NAV_PARAMS"]['nTopCount'] = intval($criteria['limit']);
        } else if (is_array($criteria['limit'])) {
            $arParameters["NAV_PARAMS"] = $criteria['limit'];
        }
        if ($notAdmins === true) {
            $arFilter['!GROUPS_ID'] = array(self::ROLE_ADMIN);
        }


        if ($this->obCache->InitCache($this->cacheTime, $cacheId, $this->cachePath)) {
            $vars = $this->obCache->GetVars();
            $users = $vars[$cacheId];
        } elseif ($this->obCache->StartDataCache()) {
            $rsUsers = \CUser::GetList($by, $order, $arFilter, $arParameters); // выбираем пользователей
            // параметры ограничения выборки
            $page = (intval($criteria['page']))
                ? : false;
            $perPage = (intval($criteria['perPage']))
                ? : false;
            if ($page && $perPage) {
                $rsUsers->NavStart($perPage, false, $page);
            }
            while ($arUser = $rsUsers->GetNext()) {
                $arUser = $this->additionalFields($arUser);

                if ($callback) {
                    call_user_func_array($callback, array(&$arUser));
                }
                $users[$arUser['ID']] = $arUser;

            }
            $this->obCache->EndDataCache(array($cacheId => $users));
        }
        return $users;
    }

    /**
     * Возвращает количество пользователей по фильтру
     *
     * @param array $criteria
     * @param bool  $notAdmins
     *
     * @return bool|int
     */
    public function getUsersCount($criteria = array(), $notAdmins = false)
    {
        $arFilter = ($criteria['filter'])
            ? $criteria['filter']
            : array();
        if ($notAdmins === true) {
            $arFilter['!GROUPS_ID'] = array(self::ROLE_ADMIN);
        }
        $rsUsers = \CUser::GetList($by = 'id', $order = 'desc', $arFilter); // выбираем пользователей
        return $rsUsers->SelectedRowsCount();
    }

    /**
     * Проверяет заполнен профиль или нет
     *
     * @param bool $force Делать повторный запрос к базе или нет
     *
     * @return bool
     */
    public function profileIsComplete($force = false)
    {
        global $USER;
        $profile = $this->getProfile(null, $force);
        return ($profile['UF_PROFILE_COMPLETE'] === 'Y'
                && !empty($profile['FULL_NAME'])
                || $USER->IsAdmin());
    }

    /**
     * Возвращает профиль пользователя.
     *
     * @param int  $uid Идентификатор пользователя, необязательный, по умолчанию текущий пользователь
     * @param bool $force Не кешировать получение профиля
     *
     * @return array
     */
    public function getProfile($uid = null, $force = false)
    {
        if (is_null($uid)) {
            global $USER;
            $uid = intval($USER->GetID());
        } else {
            $uid = intval($uid);
        }
        if (!$uid) {
            return array();
        }
        if (!isset($this->profiles[$uid]) || $force) {
            $profile = array();
            $rsUser = \CUser::GetByID($uid);
            $arUser = $rsUser->Fetch();

            foreach ($arUser as $key => $value) {
                $profile[$key] = $this->safeValue($value);
            }

            if (!empty($profile)) {
                $profile = $this->additionalFields($profile);
            }

            $this->profiles[$uid] = $profile;
        }

        return $this->profiles[$uid];
    }

    /**
     * Экранирование html-сущностей строк
     *
     * @param $value
     *
     * @return string
     */
    protected function safeValue($value)
    {
        return (is_string($value))
            ? htmlentities($value, ENT_QUOTES, 'UTF-8')
            : $value;
    }

    /**
     * Дополнительные поля пользователя
     *
     * @param $profile
     *
     * @return mixed
     */
    protected function additionalFields($profile) {
        $profile['FULL_NAME'] = $this->getFullName($profile['NAME'], $profile['LAST_NAME']);
        return $profile;
    }

    /**
     * Полное имя пользователя
     *
     * @param $firstName
     * @param $lastName
     *
     * @return string
     */
    protected function getFullName($firstName, $lastName)
    {
        $fullName = trim($firstName);
        $fullName .= ($lastName !== '')
            ? ' ' . $lastName
            : '';
        return $fullName;
    }

    /**
     * Доступ к значению пользовательского поля по его ID
     *
     * @param $id
     *
     * @return mixed
     */
    public function userFieldValue($id)
    {
        if (intval($id) > 0) {
            $userField = \CUserFieldEnum::GetList(array(), array("ID" => intval($id)));
            if ($userFieldAr = $userField->GetNext()) {
                return $userFieldAr["VALUE"];
            }
        }

        return false;
    }

    /**
     * Доступ к спискам в пользовательских полях по названию поля. Обертка над \CUserFieldEnum::GetList
     *
     * @param $fieldName
     *
     * @return array
     */
    public function userFieldEnum($fieldName)
    {
        $rsEnum = \CUserFieldEnum::GetList(array(), array('USER_FIELD_NAME' => $fieldName));
        $enums = array();
        while ($arEnum = $rsEnum->GetNext()) {
            $enums[$arEnum['ID']] = $arEnum['VALUE'];
        };
        return $enums;
    }

    /**
     * Сохраняет пользовательское свойство
     *
     * @param string $code Код свойства
     * @param mixed $value Значение свойства
     * @param null|int $uid Идентификатор пользователя
     *
     * @return bool
     */
    public function saveProp($code, $value, $uid = null)
    {
        if (is_null($uid)) {
            global $USER;
            $uid = intval($USER->GetID());
        } else {
            $uid = intval($uid);
        }
        $data = array(
            'ID' =>  $uid,
            $code => $value,
        );
        $this->setData($data);
        return $this->save(false);
    }

    /**
     * Сохранение элемента
     *
     * @return bool
     */
    protected function saveElement()
    {
        $oUser = new \CUser;
        $data = $this->getData();
        if ($this->isNewElement()) {
            $newId = $oUser->Add($data);
        } else {
            $result = $oUser->Update($this->getAttrValue('ID'), $data);
            if ($result) {
                $newId = $this->getAttrValue('ID');
            } else {
                $newId = null;
            }
        }

        if ($newId) {
            $this->setAttrValue('ID', $newId);
            $this->clearCache();
            return true;
        } else {
            Message::getInstance()->addMessage(
                $oUser->LAST_ERROR,
                Message::ERROR
            );
            $this->lastError = $oUser->LAST_ERROR;
            return false;
        }
    }

    /**
     * Находит название атрибута в базе
     */
    protected function findAttrNames()
    {
        $this->attrNames = $this->labels();
        $rsData = \CUserTypeEntity::GetList(array('ID' => 'ASC'), array('ENTITY_ID' => 'USER', 'LANG' => 'ru'));
        while ($arRes = $rsData->Fetch()) {
            $this->attrNames[$arRes["FIELD_NAME"]] = ($arRes['EDIT_FORM_LABEL'])
                ? : $arRes["FIELD_NAME"];
        }
    }

    /**
     * Подписи стандартных полей пользователя
     *
     * @return array
     */
    protected function labels()
    {
        return array(
            'ID' => 'Идентификатор пользователя',
            'TIMESTAMP_X' => 'Последнее изменение',
            'LOGIN' => 'Имя входа',
            'PASSWORD' => 'Хеш от пароля',
            'CHECKWORD' => 'Контрольная строка для смены пароля',
            'ACTIVE' => 'Активность',
            'NAME' => 'Имя',
            'LAST_NAME' => 'Фамилия',
            'SECOND_NAME' => 'Отчество',
            'EMAIL' => 'E-mail адрес',
            'LAST_LOGIN' => 'Дата последней авторизации',
            'LAST_ACTIVITY_DATE' => 'Дата последнего хита на сайте',
            'DATE_REGISTER' => 'Дата регистрации',
            'LID' => 'сайта по умолчанию для уведомлений',
            'ADMIN_NOTES' => 'Заметки администратора',
            'EXTERNAL_AUTH_ID' => 'Код источника Внешней авторизации',
            'PERSONAL_PROFESSION' => 'Профессия',
            'PERSONAL_WWW' => 'Web-страница',
            'PERSONAL_ICQ' => 'ICQ',
            'PERSONAL_GENDER' => 'Пол',
            'PERSONAL_BIRTHDAY' => 'Дата рождения',
            'PERSONAL_PHOTO' => 'Фотография',
            'PERSONAL_PHONE' => 'Телефон',
            'PERSONAL_FAX' => 'Факс',
            'PERSONAL_MOBILE' => 'Мобильный телефон',
            'PERSONAL_PAGER' => 'Пэйджер',
            'PERSONAL_STREET' => 'Улица, дом',
            'PERSONAL_MAILBOX' => 'Почтовый ящик',
            'PERSONAL_CITY' => 'Город',
            'PERSONAL_STATE' => 'Область / край',
            'PERSONAL_ZIP' => 'Индекс',
            'PERSONAL_COUNTRY' => 'Страна',
            'PERSONAL_NOTES' => 'Дополнительные заметки',
            'WORK_COMPANY' => 'Наименование компании',
            'WORK_DEPARTMENT' => 'Департамент / Отдел',
            'WORK_POSITION' => 'Должность',
            'WORK_WWW' => 'Web-страница',
            'WORK_PHONE' => 'Телефон',
            'WORK_FAX' => 'Факс',
            'WORK_PAGER' => 'Пэйджер',
            'WORK_STREET' => 'Улица, дом',
            'WORK_MAILBOX' => 'Почтовый ящик',
            'WORK_CITY' => 'Город',
            'WORK_STATE' => 'Область / край',
            'WORK_ZIP' => 'Индекс',
            'WORK_COUNTRY' => 'Страна',
            'WORK_PROFILE' => 'Направления деятельности',
            'WORK_LOGO' => 'Логотип',
            'WORK_NOTES' => 'Дополнительные заметки',
            'GROUP_ID' => 'Группа пользователя',
        );
    }
}