<?
/*
You can place here your functions and event handlers

AddEventHandler("module", "EventName", "FunctionName");
function FunctionName(params)
{
	//code
}
*/
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ERROR | E_PARSE);

define('DS', DIRECTORY_SEPARATOR);

spl_autoload_register(
    function ($class) {
        $class = str_replace('\\', '/', $class);
        include $_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/classes/" . $class . '.php';
    }
);

if (!function_exists('dump')) {
    function dump($data, $name = '', $forAdminOnly = true)
    {
        $debug = debug_backtrace();
        $file = str_replace($_SERVER['DOCUMENT_ROOT'], '', $debug[0]['file']);
        $line = $debug[0]['line'];

        global $USER;
        if ($forAdminOnly && !$USER->IsAdmin()) {
            return;
        }
        $name = ($name !== '')
            ? $name . ' : '
            : $name;
        echo "<pre class='dump'><small class='pre__title'>{$file} - {$line}</small>\n" . '<span class="pre__name">' . $name . '</span>'; print_r($data); echo "</pre>";
    }
}

$eventHandlers = new \F5Studio\EventHandlers();

?>