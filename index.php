<?
use F5Studio\Parser\DOCXParser;
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Демонстрационная версия продукта «1С-Битрикс: Управление сайтом»");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Главная страница");
use XMLReader;
use F5Studio\Parser\F5Studio\Parser;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Reader\Word2007;
use PhpOffice\PhpWord\Writer\PDF;
use PhpOffice\PhpWord\Reader\RTF;
use PhpOffice\PhpWord\Reader\PhpOffice\PhpWord\Reader;
use F5Studio\Parser\docx2html;
?>
<?
//TODO: Перенести в событие на изменение элемента ИБ Иски
/*
 * include("bitrix/php_interface/classes/F5Studio/mpdf/mpdf.php");
 * $doc = new DOCXParser();
 * $doc->setFile('2.docx');
 *
 * $html = $doc->to_html();
 * echo $html;
 * $plain_text = $doc->to_plain_text();
 * echo "<br>";
 * echo $plain_text;
 */
// load lib
$criteria = array(
    'filter' => array(
        "ID" => 3
    )
);
$typeIsk = new F5Studio\DataMapper\IBlock\IskType();
//$arType = $typeIsk->getItem($criteria, true);

$doc = new DOCXParser();
$doc->setFile('2.docx');
$content = $doc->to_plain_text();
// save content to file
file_put_contents('test_document.html', $html);
$arForForm = $doc->getArrayFromStringByTags($content);
$arForm = $doc->getFormArray($arForForm);
$typeIsk->updateProperty('FORM_STEPS', $arForm, 3);

// Example Docx file path
$dirDocxFilePath = sprintf('2.docx', dirname(__FILE__));
// Get an instance of the docx2html class
$resMyDoc = new docx2html();
// Load in the demo docx file
$resMyDoc->load($dirDocxFilePath);
// echo 'DocX HTML Content:<br><pre>'.$resMyDoc->html().'</pre><hr>';
// echo 'DocX XML Content:<br><pre>'.htmlentities($resMyDoc->xml()).'</pre><hr>';
$docxString = $resMyDoc->plain();
echo 'DocX Plain Content:<br><pre>' . $resMyDoc->plain() . '</pre><hr>';
preg_match_all('/\[HEADER\](.*)\[\/HEADER\]/si', $docxString, $arHeader);
echo 'HEADER:<br><pre>';
if ($typeIsk->updateProperty('HEADER', $arHeader[1][0], 3))
    print_r($arHeader);
echo "<br>";
echo "<br>";
preg_match_all('/\[COMMON_PART_START\](.*)\[\/COMMON_PART_START\]/si', $docxString, $arCSS);
echo 'COMMON_PART_START:<br><pre>';
if ($typeIsk->updateProperty('COMMON_PART_START', $arCSS[1][0], 3))
    print_r($arCSS);

echo "<br>";
echo "<br>";
preg_match_all('/\[COMMON_PART_BODY\](.*)\[\/COMMON_PART_BODY\]/si', $docxString, $arCSS);
echo 'COMMON_PART_BODY:<br><pre>';
if ($typeIsk->updateProperty('COMMON_PART_BODY', $arCSS[1][0], 3))
    print_r($arCSS);
echo "<br>";
echo "<br>";
preg_match_all('/\[COMMON_PART_END\](.*)\[\/COMMON_PART_END\]/si', $docxString, $arCSS);
echo 'COMMON_PART_END:<br><pre>';
if ($typeIsk->updateProperty('COMMON_PART_END', $arCSS[1][0], 3))
    print_r($arCSS);
echo "<br>";
echo "<br>";
preg_match_all('/\[SIGNATURE\](.*)\[\/SIGNATURE\]/si', $docxString, $arCSS);
echo 'SIGNATURE:<br><pre>';
if ($typeIsk->updateProperty('SIGNATURE', $arCSS[1][0], 3))
    print_r($arCSS);
echo "<br>";
echo "<br>";
$arType = $typeIsk->getItem($criteria, true);
print_r($arType);
// print_r(explode(PHP_EOL, $resMyDoc->plain()));
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>