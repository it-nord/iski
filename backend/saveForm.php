<?
use F5Studio\CH;
use F5Studio\DataMapper\IBlock\Isk;
use F5Studio\DataMapper\IBlock\IskType;
use F5Studio\DataMapper\IBlock\Poshlina;
use F5Studio\Parser\ParserHelper;
use F5Studio\IskGenerator\PDFGenerator;
require_once ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
parse_str($_POST['formData'], $formData);
foreach ($formData as $k => $v) {
    $_POST[$k] = $v;
}
$iskPrice = $_POST['ISK_PRICE'];
$model = new Poshlina();
$gosPoshlina = $model->getGPbySum($iskPrice);
$_POST["form"]["ISK_GP"] = $gosPoshlina;
$fields = json_encode($_POST);
$isk = new Isk();
$data = array(
    "NAME" => "Исковое заявление",
    "PROPERTY_VALUES" => array(
        "FIELDS" => $fields
    )
);
$isk->setData($data);
$isk->save(false);
$iskId = $isk->getData();
$criteria = array(
    'filter' => array(
        "ID" => $iskId["ID"]
    )
);
$pHelper = new ParserHelper();
$isk = new Isk();
$arIsk = $isk->getItem($criteria, true);
$arJson = CH::getJsonDecodeWithoutBom($arIsk['FIELDS']['~VALUE']['TEXT']);
$criteria = array(
    'filter' => array(
        "ID" => $arIsk['ISK_TYPE']['VALUE']
    )
);
$iskType = new IskType();
$arType = $iskType->getItem($criteria, true);
$html = $pHelper->getTexBlock($arType['HEADER']['~VALUE']['TEXT'], $arJson['form'], "<div><table width=\"100%\"><tr><td></td><td width=\"250\"><div>", "</div></td></tr></table>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType['COMMON_PART_START']['~VALUE']['TEXT'], $arJson['form'], "<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType['COMMON_PART_BODY']['~VALUE']['TEXT'], $arJson['form'], "<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType['COMMON_PART_END']['~VALUE']['TEXT'], $arJson['form'], "<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType['COMMON_PART_ATTACH']['~VALUE']['TEXT'], $arJson['form'], "<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
$html .= $pHelper->getTexBlock($arType['SIGNATURE']['~VALUE']['TEXT'], $arJson['form'], "<div class=\"body\">", "</div>");
$html = $pHelper->replaceDefinitions($html, $arJson['form']);
$html = $pHelper->replaceRepStrings($html, $arJson['form']);
$html = $pHelper->replaceSimpleTags($html, $arJson['form']);
$html = $pHelper->replaceMathExpressions($html, $arJson['form']);
require_once ($_SERVER["DOCUMENT_ROOT"] . '/bitrix/php_interface/classes/tcpdf/tcpdf.php');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// add a page
$pdf->AddPage();
// set font
$pdf->SetFont('times', '', 14);
// output the HTML content
$pdf->writeHTML($html, true, 0, true, true);
// ---------------------------------------------------------
// Close and output PDF document
$pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/create/' . $iskId["ID"] . '.pdf', 'F');
echo '<p><a href="/create/' . $iskId["ID"] . '.pdf" target="_blank">Скачайте Ваш иск</a></p>';
